package fxModules;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.simple.JSONObject;
import util.NetworkWorker;
import util.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by NDS on 22.04.2015.
 */
public class ModuleAdminParametrs extends BasicModule
{

    @FXML
    CheckBox checkBox_isBlocked;
    @FXML
    Button button_save;

    public static ModuleAdminParametrs create()
    {
        ModuleAdminParametrs module = new ModuleAdminParametrs();
        module.initAsFree();

        return module;
    }

    public ModuleAdminParametrs()
    {
        id = Util.ModuleID.M_LOGIN;

        titleWindow = "Параметры";

        resourseFxml = "assets/modules/mAdminParametrs_Form.fxml";
        resourseCss = null;
        controller = this;
    }

    protected void onInited()
    {
        super.onInited();


        initParametrs();

        button_save.setOnMouseClicked((event)->
        {
            saveParametrs(checkBox_isBlocked.isSelected());
            destroy();
        });
    }

    private void initParametrs()
    {
        NetworkWorker worker = new NetworkWorker("/api/get_parametrs", null, (String res)->
        {
            JSONObject json = Util.parseStringToJObject(res);
            boolean isBlocked = Boolean.parseBoolean(json.get("isBlocked").toString());

            Platform.runLater(()->
            {
                checkBox_isBlocked.setSelected(isBlocked);
            });
        });
    }

    private void saveParametrs(boolean isBlocked)
    {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("is_blocked", Boolean.toString(isBlocked)));

        NetworkWorker worker = new NetworkWorker("/api/set_parametrs", params, (String res)->
        {
            System.out.println(res);
        });
    }
}
