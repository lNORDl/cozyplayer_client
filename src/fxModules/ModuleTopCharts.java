package fxModules;

import audio.Audiotrack;
import audio.Playlist;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.chart.PieChart;
import javafx.scene.control.*;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import util.Util;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by NDS on 22.04.2015.
 */
public class ModuleTopCharts extends BasicModule
{
    @FXML
    ComboBox comboBox_count;
    @FXML
    DatePicker datePicker_start;
    @FXML
    DatePicker datePicker_end;
    @FXML
    RadioButton radioButton_audiotracks;
    @FXML
    RadioButton radioButton_genres;

    @FXML
    Button button_show;

    @FXML
    PieChart pieChart_chart;
    @FXML
    VBox vBox_content;

    private ModuleAudiotracksList mAudiotracksList;

    public static ModuleTopCharts create()
    {
        ModuleTopCharts module = new ModuleTopCharts();
        module.initAsFree();

        return module;
    }

    public ModuleTopCharts()
    {
        titleWindow = "Топы";

        resourseFxml = "assets/modules/mTopCharts_Form.fxml";
        resourseCss = Util.getPath("assets/css/MainStyles.css");
        controller = this;
    }

    protected void onInited()
    {
        super.onInited();


        comboBox_count.setItems(FXCollections.observableArrayList(1, 2, 3, 5, 10));
        comboBox_count.getSelectionModel().select(2);

        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        datePicker_start.getEditor().setText(dateFormat.format(new Date()));
        datePicker_end.getEditor().setText(dateFormat.format(new Date()));

        button_show.setOnMouseClicked((event)->
        {
            int count = (Integer) comboBox_count.getSelectionModel().getSelectedItem();
            String dateStart = datePicker_start.getEditor().getText();
            String dateEnd= datePicker_end.getEditor().getText();

            if(radioButton_audiotracks.isSelected()) showAudiotracksTop(count, dateStart, dateEnd);
            if(radioButton_genres.isSelected()) showGenresTop(count, dateStart, dateEnd);
        });
    }

    private void showAudiotracksTop(int count, String dateStart, String dateEnd)
    {
        pieChart_chart.getData().clear();
        if(mAudiotracksList == null) mAudiotracksList = ModuleAudiotracksList.create(vBox_content);
        VBox.setVgrow(mAudiotracksList.getRootPane(), Priority.ALWAYS);

        ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList();

        app.reqGetTopAudiotracks(count, dateStart, dateEnd, (String res)->
        {
            ArrayList<Audiotrack> tracks = new ArrayList<Audiotrack>();
            JSONArray json = Util.parseStringToJArray(res);
            for(int i = 0; i < json.size(); i++)
            {
                JSONObject data = (JSONObject) json.get(i);
                Audiotrack track = new Audiotrack(data);
                if(app.audioArchive.getUserAudiotrackById(track.getId()) != null) track = app.audioArchive.getUserAudiotrackById(track.getId());

                tracks.add(track);

                String name =  track.getName();
                int popularity = Integer.parseInt(data.get("count").toString());
                name += " [" + popularity + "]";

                pieChartData.add(new PieChart.Data(name, popularity));
            }

            Platform.runLater(() ->
            {
                pieChart_chart.setData(pieChartData);
                mAudiotracksList.show(tracks, ModuleAudiotracksList.getControlBar(1, 3));
            });
        });
    }

    private void showGenresTop(int count, String dateStart, String dateEnd)
    {
        pieChart_chart.getData().clear();
        if(mAudiotracksList != null) mAudiotracksList.destroy();
        mAudiotracksList = null;

        ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList();

        app.reqGetTopGenres(count, dateStart, dateEnd, (String res)->
        {
            JSONArray json = Util.parseStringToJArray(res);
            for(int i = 0; i < json.size(); i++)
            {
                JSONObject data = (JSONObject) json.get(i);
                String name = data.get("name").toString();
                int popularity = Integer.parseInt(data.get("count").toString());
                name += " [" + popularity + "]";

                pieChartData.add(new PieChart.Data(name, popularity));
            }

            Platform.runLater(() ->
            {
                pieChart_chart.setData(pieChartData);
            });
        });
    }
}
