package fxModules;

import audio.Playlist;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import util.Util;

import java.util.ArrayList;

/**
 * Created by NDS on 08.12.2014.
 */
public class ModuleAdminConsole extends BasicModule
{
    @FXML
    TextArea tArea_query;
    @FXML
    VBox vBox_output;
    @FXML
    Button button_prev;
    @FXML
    Button button_send;
    @FXML
    Button button_all;
    @FXML
    Button button_last;
    @FXML
    Button button_clear;
    @FXML
    Button button_next;

    private ArrayList<String> historyOutput;
    private ArrayList<String> historyQuery;
    private int historyI = 0;

    private boolean isAll;

    public static ModuleAdminConsole create()
    {
        ModuleAdminConsole module = new ModuleAdminConsole();
        module.initAsFree();

        return module;
    }

    public ModuleAdminConsole()
    {
        titleWindow = "Консоль";

        resourseFxml = "assets/modules/mAdminConsole_Form.fxml";
        resourseCss = Util.getPath("assets/css/MainStyles.css");
        controller = this;
    }

    protected void onInited()
    {
        super.onInited();


        historyOutput = new ArrayList<String>();
        historyQuery = new ArrayList<String>();


        button_send.setOnMouseClicked((event)->
        {
            String query = tArea_query.getText();
            send(query);
        });

        button_all.setOnMouseClicked((event)->
        {
            isAll = true;
            updateOutput();
        });

        button_last.setOnMouseClicked((event)->
        {
            isAll = false;
            updateOutput();
        });

        button_clear.setOnMouseClicked((event)->
        {
            historyOutput.clear();
            historyQuery.clear();
            updateOutput();
        });

        button_prev.setOnMouseClicked((event)->
        {
            if(historyI > 0 && historyQuery.size() > 0)
            {
                historyI --;
                tArea_query.setText(historyQuery.get(historyI));
            }
        });

        button_next.setOnMouseClicked((event)->
        {
            if(historyI < historyQuery.size() - 1 && historyQuery.size() > 0)
            {
                historyI ++;
                tArea_query.setText(historyQuery.get(historyI));
            }
        });
    }

    private void send(String query)
    {
        historyQuery.add(query);
        historyI = historyQuery.size()-1;
        app.reqDoAdminQuery(query, (String res)->
        {
            historyOutput.add(res);

            Platform.runLater(() ->
            {
                updateOutput();
            });
        });
    }

    private void updateOutput()
    {
        vBox_output.getChildren().clear();

        if(isAll)
        {
            for(int i = 0; i < historyOutput.size(); i++)
            {
                VBox vBox = getOutputBox(historyQuery.get(i), historyOutput.get(i));
                vBox_output.getChildren().add(vBox);
                if(i != historyOutput.size() - 1)
                {
                    Separator separator = new Separator();
                    separator.setOrientation(Orientation.HORIZONTAL);
                    vBox_output.getChildren().add(separator);
                }
            }
        }
        else
        {
            int i = historyQuery.size() - 1;
            VBox vBox = getOutputBox(historyQuery.get(i), historyOutput.get(i));
            vBox_output.getChildren().add(vBox);
        }
    }

    private VBox getOutputBox(String query, String output)
    {
        VBox vBox = new VBox();
        vBox.setPadding(new Insets(5, 5, 5, 5));
        vBox.getStyleClass().add("console-output-item");
        vBox.setMaxWidth(Double.MAX_VALUE);
        vBox.setFillWidth(true);
        Label labelQuery = new Label(query);
        labelQuery.setFont(Font.font("Calibri", FontWeight.BOLD, 14));
        Label labelOutput = new Label(output);
        vBox.getChildren().addAll(labelQuery, labelOutput);
        vBox.setOnMouseClicked((event)->
        {
            tArea_query.setText(query);
        });

        return vBox;
    }
}
