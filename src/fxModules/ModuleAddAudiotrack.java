package fxModules;

import audio.Audiotrack;
import audio.Playlist;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableArray;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.FileChooser;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import util.NetworkWorker;
import util.Util;

import java.io.File;

/**
 * Created by NDS on 03.02.2015.
 */
public class ModuleAddAudiotrack extends BasicModule
{
    @FXML
    Button button_file;
    @FXML
    TextField tField_file;
    @FXML
    TextField tField_name;
    @FXML
    TextField tField_artist;
    @FXML
    TextField tField_tags;
    @FXML
    Button button_load;
    @FXML
    CheckBox checkBox_private;
    @FXML
    ComboBox comboBox_genres;
    @FXML
    Label label_duration;

    private File file;
    private String name;
    private String artist;
    private String tags;
    private float duration;
    private boolean isPrivate;
    private int idGenre;

    private boolean isInfo;

    private JSONArray genres;

    private Playlist playlist;

    private boolean isReady;

    public static ModuleAddAudiotrack create(Playlist playlist)
    {
        ModuleAddAudiotrack module = new ModuleAddAudiotrack();
        module.playlist = playlist;
        module.initAsFree();

        return module;
    }

    public ModuleAddAudiotrack()
    {
        id = Util.ModuleID.M_ADD_AUDIOTRACK;

        titleWindow = "Добавить Аудиотрек";

        resourseFxml = "assets/modules/mAddAudiotrack_Form.fxml";
        resourseCss = null;
        controller = this;
    }

    protected void onInited()
    {
        super.onInited();


        AnchorPane.setTopAnchor(rootPane, 0.0);
        AnchorPane.setBottomAnchor(rootPane, 0.0);
        AnchorPane.setLeftAnchor(rootPane, 0.0);
        AnchorPane.setRightAnchor(rootPane, 0.0);

        button_file.setOnMouseClicked((event) ->
        {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Выбор файла");
            fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Audio Files", "*.mp3"));
            file = fileChooser.showOpenDialog(app.getStage());

            if (file != null)
            {
                isReady = false;
                tField_file.setText(file.getName());

                Media media = new Media(file.toURI().toString());
                MediaPlayer player = new MediaPlayer(media);
                player.setOnError(()->
                {
                    player.getError().printStackTrace();
                });
                player.setOnReady(()->
                {
                    isReady = true;
                    duration = Util.soundTimeToMinutes((int) media.getDuration().toSeconds());
                    if(duration < 0.01) duration = 0.01f;
                    tField_name.setText((String) media.getMetadata().get("title"));
                    tField_artist.setText((String) media.getMetadata().get("artist"));
                    label_duration.setText(duration + "");

                    player.dispose();
                });
            }
        });

        button_load.setOnMouseClicked((event)->
        {
            sendAudiotrack();
        });

        ObservableList<String> items = FXCollections.observableArrayList();
        genres = app.getGenres();
        for(int i = 0; i < genres.size(); i++)
        {
            JSONObject genre = (JSONObject) genres.get(i);
            items.add(genre.get("name").toString());
        }
        comboBox_genres.setItems(items);
        comboBox_genres.getSelectionModel().select(0);
    }

    private void sendAudiotrack()
    {
        name = tField_name.getText();
        artist = tField_artist.getText();
        tags = "";
        tags += name.toLowerCase() + ", ";
        tags += artist.toLowerCase() + ", ";
        tags += comboBox_genres.getValue().toString() + ", ";
        tags += tField_tags.getText();
        isPrivate = checkBox_private.isSelected();
        idGenre = Integer.parseInt(Util.lookup(genres, "name", comboBox_genres.getValue().toString(), "id").toString());

        boolean isComplete = (Util.checkString(name, 3, 50) &&
                              Util.checkString(artist, 3, 50) &&
                              Util.checkString(tags, 0, 100) &&
                              file != null && isReady);

        //System.out.println(isComplete + " : " + name + " : " + artist + " : " + tags + " : " + duration);
        //System.out.println(Util.getTags(tags));

        if(isComplete)
        {
            app.audioArchive.uploadNewAudiotrack(file, name, artist, duration, tags, isPrivate, idGenre, playlist);
            destroy();
            //NetworkWorker.addLoading(worker, artist + ": " + name);
        }
    }

    @Override
    public void destroy()
    {
        System.out.println("ModuleAddAudiotrack: Destroyed!");

        file = null;


        super.destroy();
    }
}
