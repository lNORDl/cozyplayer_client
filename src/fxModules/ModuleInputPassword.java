package fxModules;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import org.json.simple.JSONObject;
import util.IDo;
import util.Util;

/**
 * Created by NDS on 22.04.2015.
 */
public class ModuleInputPassword extends BasicModule
{
    @FXML
    Button button_password;
    @FXML
    TextField tField_password;

    private IDo onClose;

    public static ModuleInputPassword create()
    {
        ModuleInputPassword module = new ModuleInputPassword();
        module.initAsFree();

        return module;
    }

    public ModuleInputPassword()
    {
        titleWindow = "Введите пароль";

        resourseFxml = "assets/modules/mInputPassword_Form.fxml";
        resourseCss = null;
        controller = this;
    }

    protected void onInited()
    {
        super.onInited();


        button_password.setOnMouseClicked((event)->
        {
            destroy();
        });
    }

    public String getPassword()
    {
        return tField_password.getText();
    }

    public void setOnClose(IDo onC)
    {
        onClose = onC;
    }

    @Override
    public void destroy()
    {
        if(onClose != null) onClose.doTask();


        super.destroy();
    }
}
