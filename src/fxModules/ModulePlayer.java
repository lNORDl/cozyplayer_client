package fxModules;

import audio.Audiotrack;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.MapChangeListener;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Bounds;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.media.AudioSpectrumListener;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Paint;
import javafx.util.Duration;
import util.Util;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by NDS on 06.01.2015.
 */
public class ModulePlayer extends BasicModule
{
    public static final int PM_NORMAL = 1;
    public static final int PM_REPEAT = 2;
    public static final int PM_RANDOM = 3;

    @FXML
    ProgressBar progress_barDuration;
    @FXML
    Slider slider_barVolume;

    @FXML
    Label label_duration;
    @FXML
    Label label_name;
    @FXML
    Label label_artist;

    @FXML
    ImageView imageView_play;

    @FXML
    Button button_volume;
    @FXML
    Button button_prev;
    @FXML
    Button button_next;
    @FXML
    Button button_pm_repeat;
    @FXML
    Button button_pm_random;
    @FXML
    Button button_pm_normal;
    @FXML
    Label label_timeAlt;

    private Media media;
    private MediaPlayer mediaPlayer;
    private AudioSpectrumListener spectrumListener;

    private boolean isMedia;
    private boolean isPlay;
    private boolean isSeek;
    private boolean isActivatePopularity;
    private boolean isTimeAlt;
    private boolean isMute;

    private Image imgButtonPlay;
    private Image imgButtonPause;
    private Image imgButtonPrev;
    private Image imgButtonNext;
    private Image imgButtonVolumeOn;
    private Image imgButtonVolumeOff;
    private Image imgButtonPmNormal;
    private Image imgButtonPmRepeat;
    private Image imgButtonPmRandom;

    private Audiotrack audiotrack;
    private ArrayList<Audiotrack> audiotracks;

    private int playMode = PM_NORMAL;
    private double curDuration = 0;
    private double fullDuration = 0;
    private double volume = 0;

    private ArrayList<ModuleAudiotracksList> audiotracksLists = new ArrayList<ModuleAudiotracksList>();

    public ModulePlayer()
    {
        id = Util.ModuleID.M_PLAYER;

        resourseFxml = "assets/modules/mPlayer_Form.fxml";
        resourseCss = Util.getPath("assets/css/ModulePlayer.css");
        controller = this;

        spectrumListener = new AudioSpectrumListener()
        {
            @Override public void spectrumDataUpdate(double timestamp, double dur, float[] magnitudes, float[] phases)
            {
                curDuration = mediaPlayer.getCurrentTime().toMillis();
                if(curDuration > fullDuration) curDuration = fullDuration;
                double persentDuration = curDuration / fullDuration;
                persentDuration = persentDuration<0.01?0.01:persentDuration;

                if(media != null && !isSeek)
                {
                    label_timeAlt.setText(getDurationAsString(curDuration));
                    updateTimeAltPosition(persentDuration);

                    progress_barDuration.setProgress(persentDuration);
                    label_duration.setText(getDurationAsString(curDuration) + " | " + getDurationAsString(fullDuration));

                    if(!isActivatePopularity && persentDuration > 0.5)
                    {
                        isActivatePopularity = true;
                        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
                        String date = dateFormat.format(new Date());
                        app.reqAddPopularity(audiotrack.getId(), date, null);
                    }
                }
            }
        };

        if(app.player == null) app.player = this;
    }

    protected void onInited()
    {
        super.onInited();


        imgButtonPlay = new Image(Util.getPath("assets/images/button_play.png"));
        imgButtonPause = new Image(Util.getPath("assets/images/button_pause.png"));
        imgButtonPrev = new Image(Util.getPath("assets/images/icons/icon_player_prev.png"));
        imgButtonNext = new Image(Util.getPath("assets/images/icons/icon_player_next.png"));
        imgButtonVolumeOn = new Image(Util.getPath("assets/images/icons/icon_player_volume_on.png"));
        imgButtonVolumeOff = new Image(Util.getPath("assets/images/icons/icon_player_volume_off.png"));
        imgButtonPmNormal = new Image(Util.getPath("assets/images/icons/icon_pm_normal.png"));
        imgButtonPmRepeat = new Image(Util.getPath("assets/images/icons/icon_pm_repeat.png"));
        imgButtonPmRandom = new Image(Util.getPath("assets/images/icons/icon_pm_random.png"));

        initProgressBarDuration();

        slider_barVolume.valueProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue)->
        {
            double d = (Double) newValue / 100;
            mediaPlayer.setVolume(d);
            if(isMute) setMute(false);
            else if (d == 0) setMute(true);
        });

        imageView_play.setImage(imgButtonPlay);

        imageView_play.setOnMouseClicked((event)->
        {
            togglePlay();
        });

        button_prev.setGraphic(new ImageView(imgButtonPrev));
        button_next.setGraphic(new ImageView(imgButtonNext));
        button_volume.setGraphic(new ImageView(imgButtonVolumeOn));
        button_pm_repeat.setGraphic(new ImageView(imgButtonPmRepeat));
        button_pm_random.setGraphic(new ImageView(imgButtonPmRandom));
        button_pm_normal.setGraphic(new ImageView(imgButtonPmNormal));

        button_pm_normal.setOnMouseClicked((event)->
        {
            setPlayMode(PM_NORMAL);
        });

        button_pm_repeat.setOnMouseClicked((event)->
        {
            setPlayMode(PM_REPEAT);
        });

        button_pm_random.setOnMouseClicked((event)->
        {
            setPlayMode(PM_RANDOM);
        });

        button_next.setOnMouseClicked((event)->
        {
            initNextAudiotrack();
        });

        button_prev.setOnMouseClicked((event)->
        {
            initPrevAudiotrack();
        });

        button_volume.setOnMouseClicked((event)->
        {
            if(isMedia)
            {
                setMute(!isMute);
            }
        });

        label_timeAlt.setVisible(false);
        label_timeAlt.setMouseTransparent(true);
    }

    private void setMute(boolean isM)
    {
        if (isM)
        {
            volume = mediaPlayer.getVolume();
            mediaPlayer.setVolume(0);
            button_volume.setGraphic(new ImageView(imgButtonVolumeOff));
        }
        else
        {
            mediaPlayer.setVolume(volume);
            button_volume.setGraphic(new ImageView(imgButtonVolumeOn));
        }

        isMute = isM;
    }

    private void setPlayMode(int pM)
    {
        playMode = pM;
    }

    private void initProgressBarDuration()
    {
        progress_barDuration.setOnMousePressed((event)->
        {
            if(isMedia)
            {
                isSeek = true;
                double d = event.getX() / (progress_barDuration.getWidth() - 2);
                progress_barDuration.setProgress(d);

                double m = fullDuration * d;
                updateTimeAltPosition(d);
                label_timeAlt.setText(getDurationAsString(m));
            }
        });

        progress_barDuration.setOnMouseDragged((event)->
        {
            if(isMedia && isSeek)
            {
                double d = event.getX() / (progress_barDuration.getWidth() - 2);
                progress_barDuration.setProgress(d);

                double m = fullDuration * d;
                updateTimeAltPosition(d);
                label_timeAlt.setText(getDurationAsString(m));
            }
        });

        progress_barDuration.setOnMouseReleased((event)->
        {
            if(isMedia && isSeek)
            {
                isSeek = false;
                double d = event.getX() / (progress_barDuration.getWidth() - 2);
                progress_barDuration.setProgress(d);

                mediaPlayer.seek(Duration.millis(d * mediaPlayer.getCycleDuration().toMillis()));
            }
        });

        progress_barDuration.setOnMouseEntered((event) ->
        {
            isTimeAlt = true;
            label_timeAlt.setVisible(true);

            updateTimeAltPosition(curDuration/fullDuration);
        });

        progress_barDuration.setOnMouseExited((event)->
        {
            isTimeAlt = false;
            label_timeAlt.setVisible(false);
        });
    }

    public void initAudiotrack(Audiotrack track, ArrayList<Audiotrack> tracks)
    {
        audiotrack = track;
        audiotracks = tracks;

        isActivatePopularity = false;

            if (mediaPlayer != null)
            {
                pause();
                mediaPlayer.stop();
                mediaPlayer.setAudioSpectrumListener(null);
                mediaPlayer.dispose();

                System.out.println("MediaPlayer disposed!");
            }

            media = new Media(Util.getAudiotrackPath(audiotrack.getPath()));
            mediaPlayer = new MediaPlayer(media);

            System.out.println("Media inited!");

            mediaPlayer.setAudioSpectrumNumBands(16);
            mediaPlayer.setAudioSpectrumListener(spectrumListener);
            mediaPlayer.setAudioSpectrumInterval(1d / 30d);

            if(!isMute) mediaPlayer.setVolume(slider_barVolume.getValue() / 100);
            else mediaPlayer.setVolume(0);

        mediaPlayer.setOnReady(()->
        {
            fullDuration = mediaPlayer.getCycleDuration().toMillis();
            label_duration.setText("00:00 | " + getDurationAsString(fullDuration));
        });

        mediaPlayer.statusProperty().addListener((ObservableValue<? extends MediaPlayer.Status> observable, MediaPlayer.Status oldValue, MediaPlayer.Status newValue)->
        {
            if(newValue == MediaPlayer.Status.PLAYING)
            {
                for (int i = 0; i < audiotracksLists.size(); i++) audiotracksLists.get(i).onPlayerUpdate(this);
                imageView_play.setImage(imgButtonPause);
            }
            if(newValue == MediaPlayer.Status.PAUSED)
            {
                for (int i = 0; i < audiotracksLists.size(); i++) audiotracksLists.get(i).onPlayerUpdate(this);
                imageView_play.setImage(imgButtonPlay);
            }
        });

        mediaPlayer.setOnEndOfMedia(()->
        {
            initNextAudiotrack();
        });

        label_artist.setText(audiotrack.getArtist());
        label_name.setText(audiotrack.getName());

        isMedia = true;
    }

    public void initNextAudiotrack()
    {
        if(audiotracks != null && audiotracks.size() > 0)
        {
            if(playMode == PM_NORMAL)
            {
                int n = audiotracks.indexOf(audiotrack);
                if(n == audiotracks.size()-1) n = 0;
                else n = n + 1;

                initAudiotrack(audiotracks.get(n), audiotracks);
                play();
            }

            if(playMode == PM_REPEAT)
            {
                initAudiotrack(audiotrack, audiotracks);
                play();
            }

            if(playMode == PM_RANDOM)
            {
                int i = audiotracks.indexOf(audiotrack);
                int n = i;
                if(audiotracks.size() > 1)
                {
                    // Хе-хе
                    while (n == i) n = Util.getRandomInt(0, audiotracks.size() - 1);
                }

                initAudiotrack(audiotracks.get(n), audiotracks);
                play();
            }
        }
    }

    public void initPrevAudiotrack()
    {
        if(audiotracks != null && audiotracks.size() > 0)
        {
            int n = audiotracks.indexOf(audiotrack);
            if(n == 0) n = audiotracks.size()-1;
            else n = n - 1;

            initAudiotrack(audiotracks.get(n), audiotracks);
            play();
        }
    }

    public Audiotrack getAudiotrack()
    {
        return audiotrack;
    }

    public void togglePlay()
    {
        if(isMedia)
        {
            if(isPlay) pause();
            else play();
        }
    }

    public void play()
    {
        if(isMedia)
        {
            isPlay = true;
            mediaPlayer.play();
        }
    }

    public void pause()
    {
        if(isMedia)
        {
            isPlay = false;
            mediaPlayer.pause();
        }
    }

    public boolean isPlay()
    {
        return isPlay;
    }

    public boolean isMedia()
    {
        return isMedia;
    }

    public void addAudiotracksList(ModuleAudiotracksList module)
    {
        if(audiotracksLists.indexOf(module) == -1) audiotracksLists.add(module);
    }

    public void removeAudiotracksList(ModuleAudiotracksList module)
    {
        if(audiotracksLists.indexOf(module) != -1) audiotracksLists.remove(module);
    }

    private String getDurationAsString(double millis)
    {
        String text = "";

        int sec = (int) (millis / 1000);
        int m = sec / 60;
        if(m < 10) text += "0";
        text += m+":";
        if((sec - m*60) < 10) text += "0";
        text += sec - m*60;

        return text;
    }

    private void updateTimeAltPosition(double d)
    {
        Point2D pos = progress_barDuration.localToScene(progress_barDuration.getLayoutBounds().getMinX(), progress_barDuration.getLayoutBounds().getMinY());
        int shift = (int) Math.floor(d * progress_barDuration.getWidth() - label_timeAlt.getWidth() / 2);
        if(shift < 0) shift = 0;
        if(shift > progress_barDuration.getWidth() - label_timeAlt.getWidth()) shift = (int) (progress_barDuration.getWidth() - label_timeAlt.getWidth());

        label_timeAlt.setLayoutX(pos.getX() + shift);
        label_timeAlt.setLayoutY(24);
    }
}
