package fxModules;

import audio.Audiotrack;
import audio.Playlist;
import core.Application;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import org.json.simple.JSONArray;
import util.IAudiotracksListControlBar;
import util.Util;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by NDS on 08.12.2014.
 */
public class ModuleAudiotracksList extends BasicModule
{
    public static int BTN_ADD = 1;
    public static int BTN_REMOVE = 2;
    public static int BTN_DOWNLOAD = 3;
    //public static int BTN_ = 1;

    @FXML
    VBox vBox_content;

    private Image iconPlay;
    private Image iconPause;

    private HashMap<Audiotrack, HBox> bands = new HashMap<Audiotrack, HBox>();
    private ArrayList<Audiotrack> tracks;
    private HBox curBand;

    private Playlist playlist;

    private IAudiotracksListControlBar iCB;

    public static ModuleAudiotracksList create(Pane pane)
    {
        ModuleAudiotracksList module = new ModuleAudiotracksList();
        if(pane != null) module.initAsChildren(pane);
        else module.initAsFree();

        return module;
    }

    public ModuleAudiotracksList()
    {
        resourseFxml = "assets/modules/mAudiotracksList_Form.fxml";
        resourseCss = Util.getPath("assets/css/MainStyles.css");
        controller = this;
    }

    protected void onInited()
    {
        super.onInited();


        iconPlay = new Image(Util.getPath("assets/images/icons/icon_band_play_1.png"));
        iconPause = new Image(Util.getPath("assets/images/icons/icon_band_pause_1.png"));

        app.moduleMain.mPlayer.addAudiotracksList(this);
    }

    public void show(ArrayList<Audiotrack> t, IAudiotracksListControlBar iControlBar, Playlist p)
    {
        show(t, iControlBar);
        playlist = p;
    }

    public void show(ArrayList<Audiotrack> t, IAudiotracksListControlBar iControlBar)
    {
        playlist = null;
        tracks = t;
        iCB = iControlBar;
        vBox_content.getChildren().clear();
        bands.clear();

        for(int i = 0; i < tracks.size(); i++)
        {
            HBox trackBand = getAudiotrackBand(tracks.get(i), iControlBar);
            vBox_content.getChildren().add(trackBand);

            bands.put(tracks.get(i), trackBand);

            Separator separator = new Separator();
            separator.setOrientation(Orientation.HORIZONTAL);
            separator.setPadding(new Insets(0, 0, 0, 0));
            vBox_content.getChildren().add(separator);
        }

        onPlayerUpdate(app.moduleMain.mPlayer);
    }

    private HBox getAudiotrackBand(Audiotrack track, IAudiotracksListControlBar iControlBar)
    {
        HashMap<String, Object> data = new HashMap<String, Object>();
        HBox hBox = new HBox();
        hBox.setAlignment(Pos.CENTER);
        hBox.setPadding(new Insets(7, 5, 7, 5));
        hBox.getStyleClass().add("audiotracks-list-item-normal");
        hBox.setUserData(data);

        ImageView iV1 = new ImageView(iconPlay);
        data.put("imageViewPlay", iV1);
        Button buttonPlay = new Button("", iV1);
        buttonPlay.getStyleClass().add("button-icon");
        hBox.getChildren().add(buttonPlay);

        Label labelArtist = new Label(track.getArtist()+":");
        labelArtist.setFont(Font.font("Calibri", FontWeight.BOLD, 14));
        HBox.setMargin(labelArtist, new Insets(0, 0, 0, 10));
        Label labelName = new Label(track.getName());
        labelName.setFont(Font.font("Calibri", FontWeight.NORMAL, 14));
        HBox.setMargin(labelName, new Insets(0, 0, 0, 5));
        hBox.getChildren().addAll(labelArtist, labelName);

        AnchorPane aP = new AnchorPane();
        aP.setMaxWidth(Double.MAX_VALUE);
        HBox.setHgrow(aP, Priority.ALWAYS);
        hBox.getChildren().add(aP);

        HBox controlBox = null;
        if(iControlBar != null)
        {
            controlBox = iControlBar.getControlBar(this, track);
            hBox.getChildren().add(controlBox);
            controlBox.setVisible(false);
        }

        Label labelDuration = new Label(Float.toString(track.getDuration()));
        labelDuration.setFont(Font.font("Calibri", FontWeight.NORMAL, 14));
        HBox.setMargin(labelDuration, new Insets(0, 0, 0, 5));
        hBox.getChildren().add(labelDuration);

        hBox.setOnMouseClicked((event)->
        {
            if(app.moduleMain.mPlayer.getAudiotrack() != track) app.moduleMain.mPlayer.initAudiotrack(track, tracks);
            app.moduleMain.mPlayer.togglePlay();
            //selectBand(hBox, true);
        });

        buttonPlay.setOnMouseClicked((event)->
        {
            if(app.moduleMain.mPlayer.getAudiotrack() != track) app.moduleMain.mPlayer.initAudiotrack(track, tracks);
            app.moduleMain.mPlayer.togglePlay();
            //selectBand(hBox, true);
        });

        final HBox finalControlBox = controlBox;
        hBox.setOnMouseEntered((event)->
        {
            if(iControlBar != null) finalControlBox.setVisible(true);
        });

        hBox.setOnMouseExited((event)->
        {
            if(iControlBar != null) finalControlBox.setVisible(false);
        });
        //if(app.moduleMain.mPlayer.getAudiotrack() == track) selectBand(hBox, false);


        return hBox;
    }

    private void selectBand(HBox band)
    {
        if(this.curBand != null && curBand != band) deselectBand(curBand);
        if(curBand != band)
        {
            band.getStyleClass().remove("audiotracks-list-item-normal");
            band.getStyleClass().add("audiotracks-list-item-selected");
        }

        curBand = band;

        HashMap<String, Object> data = (HashMap<String, Object>) band.getUserData();
        ImageView iV = (ImageView) data.get("imageViewPlay");

        if(app.moduleMain.mPlayer.isPlay()) iV.setImage(iconPause);
        else iV.setImage(iconPlay);
    }

    private void deselectBand(HBox band)
    {
        band.getStyleClass().add("audiotracks-list-item-normal");
        band.getStyleClass().remove("audiotracks-list-item-selected");

        HashMap<String, Object> data = (HashMap<String, Object>) band.getUserData();
        Audiotrack track = (Audiotrack) data.get("track");
        ImageView iV = (ImageView) data.get("imageViewPlay");
        iV.setImage(iconPlay);
    }

    public void onPlayerUpdate(ModulePlayer player)
    {
        if(player.isMedia())
        {
            HBox band = bands.get(player.getAudiotrack());
            if(band != null) selectBand(band);
            else if(curBand != null) deselectBand(curBand);
        }
    }

    public Playlist getPlaylist()
    {
        return playlist;
    }

    public void updatePlaylist()
    {
        if(playlist != null) show(playlist.getAudiotracks(), iCB, playlist);
    }

    @Override
    public void destroy()
    {
        app.moduleMain.mPlayer.removeAudiotracksList(this);


        super.destroy();
    }

    public static IAudiotracksListControlBar getControlBar(int...buttons)
    {
        Application app = Application.getInstance();

        IAudiotracksListControlBar bar = (module, track)->
        {
            HBox hBox = Util.getHBox(0);
            hBox.setSpacing(7);
            hBox.setAlignment(Pos.CENTER);
            HBox.setMargin(hBox, new Insets(0, 10, 0, 10));

            for(int i:buttons)
            {
                if(i == BTN_ADD && (app.audioArchive.getUserAudiotrackById(track.getId()) == null))
                {
                    Button btnAdd = new Button("", new ImageView(Util.getPath("assets/images/icons/icon_add_1.png")));
                    btnAdd.getStyleClass().add("button-icon");
                    hBox.getChildren().add(btnAdd);

                    btnAdd.setOnMouseClicked((event)->
                    {
                        app.audioArchive.addAudiotrackInPlaylist(track, app.audioArchive.getRootPlaylist());
                        app.moduleMain.mAudiotracksList.updatePlaylist();
                        hBox.getChildren().remove(btnAdd);
                    });
                }

                if(i == BTN_REMOVE)
                {
                    Button btnRemove = new Button("", new ImageView(Util.getPath("assets/images/icons/icon_remove_4.png")));
                    btnRemove.getStyleClass().add("button-icon");
                    hBox.getChildren().add(btnRemove);

                    btnRemove.setOnMouseClicked((event)->
                    {
                        Playlist playlist = module.getPlaylist();
                        app.audioArchive.removeAudiotrackInPlaylist(track, playlist);
                        app.moduleMain.mAudiotracksList.updatePlaylist();
                    });
                }

                if(i == BTN_DOWNLOAD && (!track.getIsPrivate() || track.getIdUser() == app.getUserId()))
                {
                    Button btnDownload = new Button("", new ImageView(Util.getPath("assets/images/icons/icon_download_1.png")));
                    btnDownload.getStyleClass().add("button-icon");
                    hBox.getChildren().add(btnDownload);

                    btnDownload.setOnMouseClicked((event)->
                    {
                        String name = Util.getDownloadUrl() + track.getArtist() + " - " + track.getName() + ".mp3";
                        System.out.print(name);
                        app.downloadAudiotrack(name, track.getPath());
                    });
                }
            }

            return hBox;
        };

        return bar;
    }
}
