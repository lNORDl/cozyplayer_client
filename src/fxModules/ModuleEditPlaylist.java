package fxModules;

import audio.Audiotrack;
import audio.Playlist;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import org.json.simple.JSONArray;
import util.Util;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by NDS on 08.12.2014.
 */
public class ModuleEditPlaylist extends BasicModule
{
    @FXML
    TextField tField_name;
    @FXML
    Button button_save;
    @FXML
    Button button_showSelected;
    @FXML
    Button button_showAll;
    @FXML
    VBox vBox_audiotracks;
    @FXML
    TextField tField_find;
    @FXML
    Button button_find;
    @FXML
    RadioButton rButton_private;
    @FXML
    RadioButton rButton_public;
    @FXML
    RadioButton rButton_protected;
    @FXML
    TextField tField_password;
    @FXML
    TextField tField_tags;
    @FXML
    CheckBox checkBox_isUsePassword;
    @FXML
    CheckBox checkBox_isChangePassword;
    @FXML
    Label label_id;
    @FXML
    Button button_delete;

    private Playlist playlist;
    private ModuleSelectAudiotrack moduleSelect;

    private String sTags;

    public static ModuleEditPlaylist create(Playlist playlist)
    {
        ModuleEditPlaylist module = new ModuleEditPlaylist();
        module.playlist = playlist;
        module.initAsFree();

        return module;
    }

    public ModuleEditPlaylist()
    {
        titleWindow = "Настройки плейлиста";

        resourseFxml = "assets/modules/mEditPlaylist_Form.fxml";
        resourseCss = null;
        controller = this;
    }

    protected void onInited()
    {
        super.onInited();


        tField_name.setText(playlist.getName());

        JSONArray json = Util.parseStringToJArray(playlist.getTags());
        sTags = "";
        for (int i = 0; i < json.size(); i++)
        {
            sTags += json.get(i) + ", ";
        }

        tField_tags.setText(sTags);

        String id = "";
        for(int i = 0; i < 8-(playlist.getId()+"").length(); i++) id += "0";
        label_id.setText(id+playlist.getId());
        button_save.requestFocus();

        ArrayList<Audiotrack> all = app.audioArchive.getUserAudiotracks();
        ArrayList<Audiotrack> selected = playlist.getAudiotracks();

        moduleSelect = ModuleSelectAudiotrack.create(vBox_audiotracks, all, selected);
        moduleSelect.showSelected();
        VBox.setVgrow(moduleSelect.getRootPane(), Priority.ALWAYS);

        if(playlist.getAccess() == 50) rButton_protected.setSelected(true);
        if(playlist.getAccess() == 90) rButton_public.setSelected(true);
        checkBox_isUsePassword.setSelected(playlist.getIsPassword());

        button_showAll.setOnMouseClicked((event)->
        {
            moduleSelect.showAll();
        });

        button_showSelected.setOnMouseClicked((event)->
        {
            moduleSelect.showSelected();
        });

        button_find.setOnMouseClicked((event)->
        {
            moduleSelect.find(tField_find.getText());
        });

        button_save.setOnMouseClicked((event)->
        {
            save();
        });

        button_delete.setOnMouseClicked((event)->
        {
            app.reqRemovePlaylist(playlist.getId(), (res)->
            {
                Platform.runLater(()->
                {
                    app.audioArchive.removePlaylist(playlist);
                    app.moduleMain.mPlaylistsMenu.update();
                    destroy();
                });
            });
        });
    }

    private void download()
    {

    }

    private void save()
    {
        JSONArray onAdd = new JSONArray();
        JSONArray onRemove = new JSONArray();

        ArrayList<Audiotrack> old = playlist.getAudiotracks();
        ArrayList<Audiotrack> selected = moduleSelect.getSelected();

        playlist.clearAudiotracks();

        for(int i = 0; i < old.size(); i++)
        {
            if(selected.indexOf(old.get(i)) == -1) onRemove.add(old.get(i).getId());
        }

        for(int i = 0; i < selected.size(); i++)
        {
            onAdd.add(selected.get(i).getId());
            playlist.addAudiotrack(selected.get(i));
        }

        if(onAdd.size() > 0) app.reqAddUsingsAudiotracks(playlist.getId(), onAdd, null);
        if(onRemove.size() > 0) app.reqRemoveUsingsAudiotracks(playlist.getId(), onRemove, null);

        checkDifferences();

        app.moduleMain.mAudiotracksList.updatePlaylist();

        destroy();
    }

    private void checkDifferences()
    {
        String name = tField_name.getText();

        int access = 10;
        if(rButton_public.isSelected()) access = 90;

        boolean isPassword = checkBox_isUsePassword.isSelected();
        boolean isChangePassword = checkBox_isChangePassword.isSelected();
        String password = tField_password.getText().trim();
        String tags = tField_tags.getText();

        if((playlist.getName().compareTo(name) != 0) ||
           (playlist.getAccess() != access) ||
           (playlist.getIsPassword() != isPassword)||
           (isChangePassword) || (sTags.compareTo(tags) != 0))
        {
            //System.out.println("XX");
            app.reqEditPlaylist(playlist.getId(), name, access, isPassword, password, isChangePassword, tags, (String res) ->
            {
                playlist.init(Util.parseStringToJObject(res));
                Platform.runLater(() ->
                {
                    app.moduleMain.mPlaylistsMenu.update();
                });
            });
        }
    }
}
