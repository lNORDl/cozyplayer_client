package fxModules;

import audio.Audiotrack;
import audio.Playlist;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import util.IAudiotracksListControlBar;
import util.IPlaylistsListControlBar;
import util.Util;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by NDS on 08.12.2014.
 */
public class ModulePlaylistsList extends BasicModule
{
    @FXML
    VBox vBox_content;

    private Image iconPrivate;
    private Image iconPublic;
    private Image iconLock;
    private Image iconUnlock;
    private Image iconPlaylist;

    private HashMap<Playlist, HBox> bands = new HashMap<Playlist, HBox>();
    private ArrayList<Playlist> playlists;

    private IPlaylistsListControlBar iControlBar;

    public static ModulePlaylistsList create(Pane pane, IPlaylistsListControlBar iCB)
    {
        ModulePlaylistsList module = new ModulePlaylistsList();
        module.iControlBar = iCB;
        module.initAsChildren(pane);

        return module;
    }

    public ModulePlaylistsList()
    {
        resourseFxml = "assets/modules/mPlaylistsList_Form.fxml";
        resourseCss = null;
        controller = this;
    }

    protected void onInited()
    {
        super.onInited();


        iconPrivate = new Image(Util.getPath("assets/images/icons/icon_user_1.png"));
        iconPublic = new Image(Util.getPath("assets/images/icons/icon_public_1.png"));
        iconLock = new Image(Util.getPath("assets/images/icons/icon_lock.png"));
        iconUnlock = new Image(Util.getPath("assets/images/icons/icon_unlock.png"));
        iconPlaylist = new Image(Util.getPath("assets/images/icons/icon_playlist.png"));
    }

    public void show(ArrayList<Playlist> p)
    {
        playlists = p;
        vBox_content.getChildren().clear();
        bands.clear();

        for(int i = 0; i < playlists.size(); i++)
        {
            HBox trackBand = getPlaylistBand(playlists.get(i));
            vBox_content.getChildren().add(trackBand);

            bands.put(playlists.get(i), trackBand);

            Separator separator = new Separator();
            separator.setOrientation(Orientation.HORIZONTAL);
            separator.setPadding(new Insets(0, 0, 0, 0));
            vBox_content.getChildren().add(separator);
        }
    }

    private HBox getPlaylistBand(Playlist playlist)
    {
        HashMap<String, Object> data = new HashMap<String, Object>();
        HBox hBox = new HBox();
        hBox.setAlignment(Pos.CENTER);
        hBox.setPadding(new Insets(7, 5, 7, 5));
        hBox.getStyleClass().add("audiotracks-list-item-normal");
        hBox.setUserData(data);

        ImageView iV1 = new ImageView(iconPlaylist);
        data.put("imageViewIcon", iV1);
        hBox.getChildren().add(iV1);

        Label labelName = new Label(playlist.getName());
        labelName.setFont(Font.font("Calibri", FontWeight.NORMAL, 14));
        HBox.setMargin(labelName, new Insets(0, 0, 0, 5));
        hBox.getChildren().add(labelName);

        AnchorPane aP = new AnchorPane();
        aP.setMaxWidth(Double.MAX_VALUE);
        HBox.setHgrow(aP, Priority.ALWAYS);
        hBox.getChildren().add(aP);

        HBox controlBox = null;
        if(iControlBar != null)
        {
            controlBox = iControlBar.getControlBar(this, playlist, hBox);
            hBox.getChildren().add(controlBox);
            controlBox.setVisible(false);
        }

        ImageView iV2 = new ImageView((playlist.getIsPassword())?iconLock:iconUnlock);
        data.put("imageViewIcon2", iV2);
        hBox.getChildren().add(iV2);

        final HBox finalControlBox = controlBox;
        hBox.setOnMouseEntered((event)->
        {
            if(iControlBar != null) finalControlBox.setVisible(true);
        });

        hBox.setOnMouseExited((event)->
        {
            if(iControlBar != null) finalControlBox.setVisible(false);
        });
        //if(app.moduleMain.mPlayer.getAudiotrack() == track) selectBand(hBox, false);


        return hBox;
    }

    @Override
    public void destroy()
    {
        super.destroy();
    }
}
