package fxModules;

import audio.Audiotrack;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import util.IAudiotracksListControlBar;
import util.Util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by NDS on 22.04.2015.
 */
public class ModuleAdminAudiotracks extends BasicModule
{
    @FXML
    DatePicker datePicker_start;
    @FXML
    DatePicker datePicker_end;
    @FXML
    Button button_show;

    @FXML
    LineChart lineChart_chart;
    @FXML
    VBox vBox_content;

    private ModuleAudiotracksList mAudiotracksList;
    private IAudiotracksListControlBar iControlBar;

    public static ModuleAdminAudiotracks create()
    {
        ModuleAdminAudiotracks module = new ModuleAdminAudiotracks();
        module.initAsFree();

        return module;
    }

    public ModuleAdminAudiotracks()
    {
        titleWindow = "Загрузки Аудиозаписей";

        resourseFxml = "assets/modules/mAdminUsers_Form.fxml";
        resourseCss = Util.getPath("assets/css/MainStyles.css");
        controller = this;

        iControlBar = (module, track)->
        {
            HBox hBox = Util.getHBox(0);
            hBox.setSpacing(7);
            hBox.setAlignment(Pos.CENTER);
            HBox.setMargin(hBox, new Insets(0, 10, 0, 10));

            Button btnRemove = new Button("", new ImageView(Util.getPath("assets/images/icons/icon_remove_4.png")));
            btnRemove.getStyleClass().add("button-icon");
            hBox.getChildren().add(btnRemove);

            btnRemove.setOnMouseClicked((event)->
            {
                app.reqRemoveAudiotrack(track.getId(), null);
                app.audioArchive.removeAudiotrackInPlaylist(track, null);
                app.moduleMain.mAudiotracksList.updatePlaylist();
                hBox.getChildren().remove(btnRemove);
            });

            return hBox;
        };
    }

    protected void onInited()
    {
        super.onInited();

        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        datePicker_start.getEditor().setText(dateFormat.format(new Date()));
        datePicker_end.getEditor().setText(dateFormat.format(new Date()));

        button_show.setOnMouseClicked((event)->
        {
            String dateStart = datePicker_start.getEditor().getText();
            String dateEnd= datePicker_end.getEditor().getText();

            showStatistics(dateStart, dateEnd);
        });
    }

    private void showStatistics(String dateStart, String dateEnd)
    {
        lineChart_chart.getData().clear();
        XYChart.Series series = new XYChart.Series();
        series.setName("Колличество загрузок");

        if(mAudiotracksList == null) mAudiotracksList = ModuleAudiotracksList.create(vBox_content);
        VBox.setVgrow(mAudiotracksList.getRootPane(), Priority.ALWAYS);

        app.reqGetAudiotracksStatistics(dateStart, dateEnd, (String res) ->
        {
            JSONObject jData = Util.parseStringToJObject(res);
            JSONArray json = (JSONArray) jData.get("statistics");

            for (int i = 0; i < json.size(); i++) {
                JSONObject data = (JSONObject) json.get(i);

                String date = data.get("date").toString();
                int count = Integer.parseInt(data.get("count").toString());

                series.getData().add(new XYChart.Data(date, count));
            }

            ArrayList<Audiotrack> tracks = new ArrayList<Audiotrack>();
            json = (JSONArray) jData.get("audiotracks");
            for(int i = 0; i < json.size(); i++)
            {
                JSONObject data = (JSONObject) json.get(i);
                Audiotrack track = new Audiotrack(data);
                if(app.audioArchive.getUserAudiotrackById(track.getId()) != null) track = app.audioArchive.getUserAudiotrackById(track.getId());

                tracks.add(track);
            }

            Platform.runLater(() ->
            {
                lineChart_chart.getData().add(series);
                mAudiotracksList.show(tracks, iControlBar);
            });
        });
    }
}
