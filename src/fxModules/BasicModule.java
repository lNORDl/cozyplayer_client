package fxModules;

import core.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.stage.WindowEvent;
import util.Util;

import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * Created by NDS on 08.12.2014.
 */
public class BasicModule
{
    public static enum ModuleType {FREE, MODAL, CHILDREN};

    protected Pane rootPane;
    protected Pane parentPane;

    protected ModuleType moduleType;

    protected Stage stage;

    protected Util.ModuleID id = Util.ModuleID.M_NONE;

    protected String titleWindow;

    protected FXMLLoader loader;
    protected String resourseCss;
    protected String resourseFxml;
    protected Object controller;

    protected Application app;

    private EventHandler onCloseEvent;

    public BasicModule()
    {
        app = Application.getInstance();
    }

    public void initModule()
    {
        try
        {
            loader = new FXMLLoader();
            loader.setLocation(new File(resourseFxml).toURI().toURL());
            loader.setController(controller);
            rootPane = (Pane) loader.load();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        if(resourseCss != null)  rootPane.getStylesheets().add(resourseCss);
    }

    public void initAsFree()
    {
        initModule();

        stage = new Stage();
        stage.setScene(new Scene(rootPane));
        stage.setTitle(titleWindow);
        stage.show();

        moduleType = ModuleType.FREE;

        onInited();
    }

    public void initAsChildren(Pane parent)
    {
        initModule();

        stage = (Stage) parent.getScene().getWindow();
        parentPane = parent;
        parentPane.getChildren().add(rootPane);

        moduleType = ModuleType.CHILDREN;

        onInited();
    }

    public void initAsModal(Stage modal)
    {
        initModule();

        stage = new Stage();
        stage.setScene(new Scene(rootPane));
        stage.setTitle(titleWindow);
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(modal);
        stage.show();

        moduleType = ModuleType.MODAL;

        onInited();
    }

    protected void onInited()
    {
        app.addModule(this);

        if(stage != app.getStage())
        {
            onCloseEvent = new EventHandler<WindowEvent>()
            {
                public void handle(final WindowEvent keyEvent)
                {
                    destroy();
                }
            };

            stage.addEventHandler(WindowEvent.WINDOW_CLOSE_REQUEST, onCloseEvent);
        }

        rootPane.applyCss();
    }

    public void destroy()
    {
        app.removeModule(this);

        switch (moduleType)
        {
            case FREE:
                stage.close();
                break;
            case MODAL:
                stage.close();
                break;
            case CHILDREN:
                parentPane.getChildren().remove(rootPane);
                break;
        }

        stage.removeEventHandler(WindowEvent.WINDOW_CLOSE_REQUEST, onCloseEvent);

        stage = null;
        loader = null;
        controller = null;
        rootPane = null;
        parentPane = null;
    }

    public Pane getRootPane()
    {
        return rootPane;
    }

    public Stage getStage() { return stage; }

    public Util.ModuleID getId() { return id; }
}
