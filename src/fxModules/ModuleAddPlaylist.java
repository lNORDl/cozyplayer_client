package fxModules;

import audio.Playlist;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import util.Util;

/**
 * Created by NDS on 08.12.2014.
 */
public class ModuleAddPlaylist extends BasicModule
{
    @FXML
    TextField tField_name;
    @FXML
    Button button_save;
    @FXML
    RadioButton rButton_private;
    @FXML
    RadioButton rButton_public;
    @FXML
    RadioButton rButton_protected;
    @FXML
    TextField tField_password;
    @FXML
    CheckBox checkBox_password;

    public static ModuleAddPlaylist create()
    {
        ModuleAddPlaylist module = new ModuleAddPlaylist();
        module.initAsFree();

        return module;
    }

    public ModuleAddPlaylist()
    {
        titleWindow = "Добавить плейлист";

        resourseFxml = "assets/modules/mAddPlaylist_Form.fxml";
        resourseCss = null;
        controller = this;
    }

    protected void onInited()
    {
        super.onInited();


        button_save.setOnMouseClicked((event)->
        {
            String name = tField_name.getText();
            boolean isPassword = false;
            String password = "";
            int access = 10;
            String tags = name;

            save(name, access, isPassword, password, tags);

            destroy();
        });
    }

    private void save(String name, int access, boolean isPassword, String password, String tags)
    {
        app.reqAddPlaylist(name, access, false, password, tags, (String res)->
        {
            if(!Util.isResponseError(res))
            {
                Playlist playlist = new Playlist(Util.parseStringToJObject(res));
                app.audioArchive.addPlaylist(playlist);
                Platform.runLater(() ->
                {
                    app.moduleMain.mPlaylistsMenu.update();
                });
            }
        });
    }
}
