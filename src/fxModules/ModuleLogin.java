package fxModules;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import org.json.simple.JSONObject;
import util.Util;

/**
 * Created by NDS on 22.04.2015.
 */
public class ModuleLogin extends BasicModule
{
    @FXML
    TitledPane titledPane_login;
    @FXML
    TextField tField_log_login;
    @FXML
    TextField tField_log_password;
    @FXML
    Button button_login;
    @FXML
    Label label_log_message;

    @FXML
    TextField tField_reg_name;
    @FXML
    TextField tField_reg_login;
    @FXML
    TextField tField_reg_email;
    @FXML
    TextField tField_reg_password_1;
    @FXML
    TextField tField_reg_password_2;
    @FXML
    Button button_reg;
    @FXML
    Label label_reg_hint;

    @FXML
    TextField tField_act_login;
    @FXML
    TextField tField_act_code;
    @FXML
    Button button_act;
    @FXML
    Label label_act_hint;

    public ModuleLogin()
    {
        id = Util.ModuleID.M_LOGIN;

        titleWindow = "Авторизация";

        resourseFxml = "assets/modules/mLogin_Form.fxml";
        resourseCss = null;
        controller = this;
    }

    protected void onInited()
    {
        super.onInited();


        titledPane_login.setExpanded(true);

        tField_log_login.setText("xxx");
        tField_log_password.setText("123");

        button_login.setOnMouseClicked((event)->
        {
            String log = tField_log_login.getText();
            String password = tField_log_password.getText();

            login(log, password);
        });

        button_reg.setOnMouseClicked((event)->
        {
            String log = tField_reg_login.getText();
            String password = tField_reg_password_1.getText();
            String email = tField_reg_email.getText();

            registration(log, password, email);
        });

        button_act.setOnMouseClicked((event)->
        {
            String log = tField_act_login.getText();
            int code = Integer.parseInt(tField_act_code.getText());

            activate(log, code);
        });
    }

    private void login(String log, String password)
    {
        app.reqLogin(log, password, (String res)->
        {
            JSONObject json = Util.parseStringToJObject(res);
            if(json.get("status").toString().compareTo("Ok") == 0)
            {
                Platform.runLater(()->
                {
                    destroy();
                    int id = Integer.parseInt(json.get("id").toString());
                    int access = Integer.parseInt(json.get("access").toString());
                    app.startApplication(id, access);
                });
            }
            else
            {
                Platform.runLater(()->
                {
                    label_log_message.setText("Ошибка авторизации!");
                });
            }
        });
    }

    private void registration(String log, String password, String email)
    {
        app.reqRegistration(log, password, email, (String res)->
        {
            JSONObject json = Util.parseStringToJObject(res);
            String hint = json.get("hint").toString();
            Platform.runLater(()->
            {
                label_reg_hint.setText(hint);
            });
        });
    }

    private void activate(String log, int code)
    {
        app.reqActivate(log, code, (String res)->
        {
            JSONObject json = Util.parseStringToJObject(res);
            String hint = json.get("hint").toString();
            Platform.runLater(()->
            {
                label_act_hint.setText(hint);
            });
        });
    }
}
