package fxModules;

import audio.Audiotrack;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import util.Util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by NDS on 06.01.2015.
 */
public class ModuleSearchAudiotracks extends BasicModule
{
    @FXML
    Button button_searchAudiotracks;
    @FXML
    TextField tField_searchAudiotracks;
    @FXML
    VBox vBox_audiotracksList;
    @FXML
    CheckBox checkBox_genre;
    @FXML
    CheckBox checkBox_date;
    @FXML
    DatePicker datePicker_date;
    @FXML
    ComboBox comboBox_genres;

    private ModuleAudiotracksList mAudiotracksList;
    private JSONArray genres;

    public ModuleSearchAudiotracks()
    {
        resourseFxml = "assets/modules/mSearchAudiotracks_Form.fxml";
        resourseCss = Util.getPath("assets/css/MainStyles.css");
        controller = this;
    }

    protected void onInited()
    {
        super.onInited();


        mAudiotracksList = ModuleAudiotracksList.create(vBox_audiotracksList);
        VBox.setVgrow(mAudiotracksList.getRootPane(), Priority.ALWAYS);

        button_searchAudiotracks.setOnMouseClicked((event)->
        {
            search();
        });

        ObservableList<String> items = FXCollections.observableArrayList();
        genres = app.getGenres();
        for(int i = 0; i < genres.size(); i++)
        {
            JSONObject genre = (JSONObject) genres.get(i);
            items.add(genre.get("name").toString());
        }
        comboBox_genres.setItems(items);
        comboBox_genres.getSelectionModel().select(0);

        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Date date = new Date();
        datePicker_date.getEditor().setText(dateFormat.format(date));

        search();
    }

    private void search()
    {
        String tags = tField_searchAudiotracks.getText();
        int idGenre = Integer.parseInt(Util.lookup(genres, "name", comboBox_genres.getValue().toString(), "id").toString());
        String date = datePicker_date.getEditor().getText();
        if(!checkBox_genre.isSelected()) idGenre = -1;
        if(!checkBox_date.isSelected()) date = "";

        app.reqSearchAudiotracks(tags, idGenre, date, (String res)->
        {
            ArrayList<Audiotrack> audiotracks = new ArrayList<Audiotrack>();
            JSONArray json = Util.parseStringToJArray(res);
            for(int i = 0; i < json.size(); i++)
            {
                int id = Integer.parseInt(((JSONObject) json.get(i)).get("id").toString());
                Audiotrack track = app.audioArchive.getUserAudiotrackById(id);
                if(track == null) track = new Audiotrack((JSONObject) json.get(i));
                audiotracks.add(track);
            }

            Platform.runLater(()->
            {
                mAudiotracksList.show(audiotracks, ModuleAudiotracksList.getControlBar(1, 3));
            });
        });
    }
}