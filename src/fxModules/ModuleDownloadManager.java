package fxModules;

import audio.Audiotrack;
import audio.Playlist;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.DirectoryChooser;
import org.json.simple.JSONObject;
import util.Util;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by NDS on 22.04.2015.
 */
public class ModuleDownloadManager extends BasicModule
{
    @FXML
    TextField tField_path;
    @FXML
    Button button_path;
    @FXML
    ComboBox comboBox_playlists;
    @FXML
    Button button_load;
    @FXML
    Label label_hint;

    private File directory;

    public static ModuleDownloadManager create()
    {
        ModuleDownloadManager module = new ModuleDownloadManager();
        module.initAsFree();

        return module;
    }

    public ModuleDownloadManager()
    {
        titleWindow = "Загрузка";

        resourseFxml = "assets/modules/mDownloadManager_Form.fxml";
        resourseCss = Util.getPath("assets/css/MainStyles.css");
        controller = this;
    }

    protected void onInited()
    {
        super.onInited();


        ArrayList<Playlist> playlists = app.audioArchive.getUserPlaylists();
        comboBox_playlists.setItems(FXCollections.observableArrayList(playlists));
        comboBox_playlists.getSelectionModel().select(0);

        button_path.setOnMouseClicked((event)->
        {
            DirectoryChooser directoryChooser = new DirectoryChooser();
            //directoryChooser.setInitialDirectory(new File(this.getClass().getClassLoader().getResource("").getPath()));
            directory = directoryChooser.showDialog(getStage());
            if(directory != null) tField_path.setText(directory.getAbsolutePath());
        });

        button_load.setOnMouseClicked((event)->
        {
            if(directory != null)
            {
                Playlist playlist = (Playlist) comboBox_playlists.getSelectionModel().getSelectedItem();
                if (playlist.getIdUser() != app.getUserId()) app.audioArchive.fillPlaylist(playlist, "", null, null);
                load(playlist);
                destroy();
            }
            else
            {
                label_hint.setText("Укажите путь к директории!");
            }
        });
    }

    private void load(Playlist playlist)
    {
        File folder = new File(directory.getAbsoluteFile() + "/" + playlist.getName());
        folder.mkdir();

        ArrayList<Audiotrack> audiotracks = playlist.getAudiotracks();
        for(int i = 0; i < audiotracks.size(); i++)
        {
            Audiotrack track = audiotracks.get(i);
            if(!track.getIsPrivate())
            {
                String name = folder.getAbsolutePath() + '/' + track.getArtist() + " - " + track.getName() + ".mp3";
                app.downloadAudiotrack(name, track.getPath());
            }
        }
    }
}
