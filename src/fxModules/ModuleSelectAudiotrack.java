package fxModules;

import audio.Audiotrack;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import org.json.simple.JSONObject;
import util.Util;

import java.util.ArrayList;

/**
 * Created by NDS on 08.12.2014.
 */
public class ModuleSelectAudiotrack extends BasicModule
{
    @FXML
    VBox vBox_audiotracks;

    private static enum MODE {ALL, SELECTED};

    private ArrayList<Audiotrack> all;
    private ArrayList<Audiotrack> selected;

    private MODE mode = MODE.ALL;

    public static ModuleSelectAudiotrack create(Pane pane, ArrayList<Audiotrack> all, ArrayList<Audiotrack> selected)
    {
        ModuleSelectAudiotrack module = new ModuleSelectAudiotrack();
        module.all = all;
        module.selected = selected;
        module.initAsChildren(pane);

        return module;
    }

    public ModuleSelectAudiotrack()
    {
        resourseFxml = "assets/modules/mSelectAudiotrack_Form.fxml";
        resourseCss = Util.getPath("assets/css/MainStyles.css");
        controller = this;
    }

    protected void onInited()
    {
        super.onInited();


        updateData();
    }

    public void showAll()
    {
        mode = MODE.ALL;
        updateData();
    }

    public void showSelected()
    {
        mode = MODE.SELECTED;
        updateData();
    }

    private void updateData()
    {
        vBox_audiotracks.getChildren().clear();

        if(mode == MODE.ALL)
        {
            for(int i = 0; i < all.size(); i++)
            {
                HBox band = createBand(all.get(i), (selected.indexOf(all.get(i)) != -1));
                vBox_audiotracks.getChildren().add(band);
            }
        }

        if (mode == MODE.SELECTED)
        {
            for(int i = 0; i < selected.size(); i++)
            {
                HBox band = createBand(selected.get(i), true);
                vBox_audiotracks.getChildren().add(band);
            }
        }
    }

    public void find(String s)
    {
        s = s.trim();

        if(s.length() < 3 || s.length() > 30)
        {
            showAll();
        }
        else
        {
            vBox_audiotracks.getChildren().clear();

            for (int i = 0; i < all.size(); i++)
            {
                Audiotrack audiotrack = all.get(i);
                if (audiotrack.getName().toLowerCase().contains(s.toLowerCase()) || audiotrack.getArtist().toLowerCase().contains(s.toLowerCase()))
                {
                    HBox band = createBand(all.get(i), (selected.indexOf(all.get(i)) != -1));
                    vBox_audiotracks.getChildren().add(band);
                }
            }
        }
    }

    public ArrayList<Audiotrack> getSelected()
    {
        return selected;
    }

    private HBox createBand(Audiotrack audiotrack, Boolean isSelected)
    {
        HBox hBox = new HBox();
        hBox.setPrefWidth(Region.USE_COMPUTED_SIZE);
        hBox.setPrefHeight(Region.USE_COMPUTED_SIZE);
        hBox.getStyleClass().add("list-item");
        hBox.setPadding(new Insets(5, 5, 5, 5));

        CheckBox checkBox = new CheckBox("");
        checkBox.setId("checkBox_selected");
        checkBox.setSelected(isSelected);
        hBox.getChildren().add(checkBox);

        Label labelArtist = new Label(audiotrack.getArtist() + ":");
        labelArtist.setFont(Font.font("Calibri", FontWeight.BOLD, 14));
        hBox.getChildren().add(labelArtist);

        Label labelName = new Label(audiotrack.getName());
        labelName.setFont(Font.font("Calibri", 14));
        HBox.setMargin(labelName, new Insets(0, 0, 0, 5));
        hBox.getChildren().add(labelName);

        AnchorPane aPane = new AnchorPane();
        aPane.setPrefWidth(Region.USE_COMPUTED_SIZE);
        aPane.setPrefHeight(Region.USE_COMPUTED_SIZE);
        aPane.setMaxWidth(Double.MAX_VALUE);
        HBox.setHgrow(aPane, Priority.ALWAYS);
        hBox.getChildren().add(aPane);

        Label labelDuration = new Label(Float.toString(audiotrack.getDuration()));
        labelDuration.setFont(Font.font("Calibri", 14));
        hBox.getChildren().add(labelDuration);

        checkBox.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue)
            {
                if(newValue) selected.add(audiotrack);
                else selected.remove(audiotrack);
            }
        });

        hBox.setOnMouseClicked((event)->
        {
            //for (int i = 0; i < selected.size(); i++) selected.get(i).printInfo();
            checkBox.setSelected(!checkBox.isSelected());
            updateData();
        });

        return hBox;
    }
}
