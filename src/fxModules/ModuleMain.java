package fxModules;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.WindowEvent;
import util.Util;

import java.io.IOException;

/**
 * Created by NDS on 08.12.2014.
 */
public class ModuleMain extends BasicModule
{
    @FXML
    VBox vBox_top;
    @FXML
    VBox vBox_left;
    @FXML
    VBox vBox_center;
    @FXML
    MenuBar menuBar_main;


    public ModulePlayer mPlayer;
    public ModulePlaylistsMenu mPlaylistsMenu;
    public ModuleAudiotracksList mAudiotracksList;

    public ModuleMain()
    {
        id = Util.ModuleID.M_MAIN;

        titleWindow = "Аудио-архив";

        resourseFxml = "assets/modules/mMain_Form.fxml";
        resourseCss = Util.getPath("assets/css/MainStyles.css");
        controller = this;
    }

    protected void onInited()
    {
        super.onInited();


        mPlayer = new ModulePlayer();
        mPlayer.initAsChildren(vBox_top);
        VBox.setVgrow(mPlayer.getRootPane(), Priority.ALWAYS);

        mPlaylistsMenu = new ModulePlaylistsMenu();
        mPlaylistsMenu.initAsChildren(vBox_left);
        VBox.setVgrow(mPlaylistsMenu.getRootPane(), Priority.ALWAYS);

        mAudiotracksList = ModuleAudiotracksList.create(vBox_center);
        VBox.setVgrow(mAudiotracksList.getRootPane(), Priority.ALWAYS);

        initMenu(app.getUserAccess());

        stage.addEventHandler(WindowEvent.WINDOW_CLOSE_REQUEST, (WindowEvent event)->
        {
            app.close();
        });
    }

    private void initMenu(int access)
    {

        if(access == Util.getAccessAdmin())
        {
            menuBar_main.getMenus().get(3).setVisible(true);

            menuBar_main.getMenus().get(3).getItems().get(0).setOnAction((event)->
            {
                ModuleAdminAudiotracks.create();
            });

            menuBar_main.getMenus().get(3).getItems().get(1).setOnAction((event)->
            {
                ModuleAdminUsers.create();
            });

            menuBar_main.getMenus().get(3).getItems().get(2).setOnAction((event)->
            {
                ModuleAdminConsole.create();
            });

            menuBar_main.getMenus().get(3).getItems().get(3).setOnAction((event)->
            {
                ModuleAdminParametrs.create();
            });
        }
        else
        {
            menuBar_main.getMenus().get(4).getItems().get(1).setVisible(false);
        }

        menuBar_main.getMenus().get(0).getItems().get(0).setOnAction((event)->
        {
            app.close();
        });

        menuBar_main.getMenus().get(1).getItems().get(0).setOnAction((event)->
        {
            ModuleSearchAudiotracks mSearch = new ModuleSearchAudiotracks();
            mSearch.initAsFree();
        });

        menuBar_main.getMenus().get(1).getItems().get(1).setOnAction((event)->
        {
            ModuleSearchPlaylists mSearch = new ModuleSearchPlaylists();
            mSearch.initAsFree();
        });

        menuBar_main.getMenus().get(1).getItems().get(3).setOnAction((event)->
        {
            ModuleTopCharts.create();
        });

        menuBar_main.getMenus().get(2).getItems().get(0).setOnAction((event)->
        {
            ModuleDownloadManager.create();
        });

        menuBar_main.getMenus().get(4).getItems().get(0).setOnAction((event)->
        {
            try {
                new ProcessBuilder("cmd", "/c","start", Util.getPath("assets/help/manual-user.chm")).start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        menuBar_main.getMenus().get(4).getItems().get(1).setOnAction((event)->
        {
            try {
                new ProcessBuilder("cmd", "/c","start", Util.getPath("assets/help/manual-admin.chm")).start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public Process exec(String cmd, boolean wait) {
        Process p;
        try {
            p=Runtime.getRuntime().exec(cmd);
//если все равно глючит хрень ниже надо раскомментировать
//p.getInputStream().close();
//p.getOutputStream().close();
//p.getErrorStream().close();
        }
        catch(java.io.IOException e) {
            //System.out.println('Can'not execute: '+cmd);
            return null;
        }
        if (wait) {
            try {
                p.waitFor();
            }
            catch(java.lang.InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
        return p;
    }
}