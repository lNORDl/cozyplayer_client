package fxModules;

import audio.Audiotrack;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.layout.VBox;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import util.Util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by NDS on 22.04.2015.
 */
public class ModuleAdminUsers extends BasicModule
{
    @FXML
    DatePicker datePicker_start;
    @FXML
    DatePicker datePicker_end;
    @FXML
    Button button_show;

    @FXML
    LineChart lineChart_chart;
    @FXML
    VBox vBox_content;

    private ModuleAudiotracksList mAudiotracksList;

    public static ModuleAdminUsers create()
    {
        ModuleAdminUsers module = new ModuleAdminUsers();
        module.initAsFree();

        return module;
    }

    public ModuleAdminUsers()
    {
        titleWindow = "Пользователи";

        resourseFxml = "assets/modules/mAdminUsers_Form.fxml";
        resourseCss = Util.getPath("assets/css/MainStyles.css");
        controller = this;
    }

    protected void onInited()
    {
        super.onInited();

        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        datePicker_start.getEditor().setText(dateFormat.format(new Date()));
        datePicker_end.getEditor().setText(dateFormat.format(new Date()));

        button_show.setOnMouseClicked((event)->
        {
            String dateStart = datePicker_start.getEditor().getText();
            String dateEnd= datePicker_end.getEditor().getText();

            showStatistics(dateStart, dateEnd);
        });
    }

    private void showStatistics(String dateStart, String dateEnd)
    {
        lineChart_chart.getData().clear();
        XYChart.Series series = new XYChart.Series();
        series.setName("Регистрации пользователей");

        app.reqGetUsersStatistics(dateStart, dateEnd, (String res) ->
        {
            ArrayList<Audiotrack> tracks = new ArrayList<Audiotrack>();
            JSONArray json = Util.parseStringToJArray(res);
            for (int i = 0; i < json.size(); i++) {
                JSONObject data = (JSONObject) json.get(i);

                String date = data.get("date").toString();
                int count = Integer.parseInt(data.get("count").toString());

                series.getData().add(new XYChart.Data(date, count));
            }

            Platform.runLater(() ->
            {
                lineChart_chart.getData().add(series);
            });
        });
    }
}
