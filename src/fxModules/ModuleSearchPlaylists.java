package fxModules;

import audio.Playlist;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import util.IAudiotracksListControlBar;
import util.Util;

import java.util.ArrayList;

/**
 * Created by NDS on 06.01.2015.
 */
public class ModuleSearchPlaylists extends BasicModule
{
    @FXML
    Button button_search;
    @FXML
    TextField tField_search;
    @FXML
    VBox vBox_playlistsList;
    @FXML
    CheckBox checkBox_isFree;
    @FXML
    VBox vBox_audiotracksList;

    private ModulePlaylistsList mPlaylistsList;
    private ModuleAudiotracksList mAudiotracksList;

    public ModuleSearchPlaylists()
    {
        resourseFxml = "assets/modules/mSearchPlaylists_Form.fxml";
        resourseCss = Util.getPath("assets/css/MainStyles.css");
        controller = this;
    }

    protected void onInited()
    {
        super.onInited();


        mPlaylistsList = ModulePlaylistsList.create(vBox_playlistsList, (module, playlist, band)->
        {
            HBox hBox = Util.getHBox(0);
            hBox.setSpacing(7);
            hBox.setAlignment(Pos.CENTER);
            HBox.setMargin(hBox, new Insets(0, 10, 0, 10));

            if(app.audioArchive.getUserPlaylistById(playlist.getId()) == null)
            {
                Button btnAdd = new Button("", new ImageView(Util.getPath("assets/images/icons/icon_add_1.png")));
                btnAdd.getStyleClass().add("button-icon");
                hBox.getChildren().add(btnAdd);

                btnAdd.setOnMouseClicked((event) ->
                {
                    hBox.getChildren().remove(btnAdd);
                    app.audioArchive.addPlaylist(playlist);
                    app.reqAddUsingPlaylist(playlist.getId(), "", null);
                    app.moduleMain.mPlaylistsMenu.update();
                });
            }

            band.setOnMouseClicked((event)->
            {
                IAudiotracksListControlBar iControlBar = ModuleAudiotracksList.getControlBar(1, 3);
                if(playlist.getIsPassword() && app.getUserId() != playlist.getIdUser())
                {
                    ModuleInputPassword mPassword = ModuleInputPassword.create();
                    mPassword.setOnClose(() ->
                    {
                        String password = mPassword.getPassword();
                        app.audioArchive.fillPlaylist(playlist, password, () ->
                        {
                            Platform.runLater(() ->
                            {
                                mAudiotracksList.show(playlist.getAudiotracks(), iControlBar, playlist);
                            });
                        }, null);
                    });
                }
                else
                {
                    app.audioArchive.fillPlaylist(playlist, "", () ->
                    {
                        Platform.runLater(() ->
                        {
                            mAudiotracksList.show(playlist.getAudiotracks(), iControlBar, playlist);
                        });
                    }, null);
                }
            });

            return hBox;
        });
        VBox.setVgrow(mPlaylistsList.getRootPane(), Priority.ALWAYS);

        mAudiotracksList = ModuleAudiotracksList.create(vBox_audiotracksList);
        VBox.setVgrow(mAudiotracksList.getRootPane(), Priority.ALWAYS);

        search();

        button_search.setOnMouseClicked((event)->
        {
            search();
        });
    }

    private void search()
    {
        String tags = tField_search.getText();
        boolean isFree = checkBox_isFree.isSelected();

        app.reqSearchPlaylists(tags, isFree, (String res)->
        {
            ArrayList<Playlist> playlists = new ArrayList<Playlist>();

            JSONArray json = Util.parseStringToJArray(res);
            for(int i = 0; i < json.size(); i++)
            {
                Playlist playlist = new Playlist((JSONObject) json.get(i));
                playlists.add(playlist);

                app.audioArchive.fillPlaylist(playlist, "111", null, null);
            }

            Platform.runLater(()->
            {
                mPlaylistsList.show(playlists);
            });
        });
    }
}