package fxModules;

import audio.Playlist;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import util.Util;

import java.util.ArrayList;

/**
 * Created by NDS on 12.05.2015.
 */
public class ModulePlaylistsMenu extends BasicModule
{
    @FXML
    VBox vBox_playlists;

    public ModulePlaylistsMenu()
    {
        resourseFxml = "assets/modules/mPlaylistsMenu_Form.fxml";
        resourseCss = null;
        controller = this;
    }

    protected void onInited()
    {
        super.onInited();


        //buildPlaylists(vBox_playlists);
    }

    public void update()
    {
        vBox_playlists.getChildren().clear();
        buildPlaylists(vBox_playlists);
    }

    private void buildPlaylists(VBox vBox)
    {
        vBox.setPadding(new Insets(0));
        vBox.getStyleClass().add("root");
        //vBox.getStylesheets().add(Util.getPath("assets/css/PlaylistsList.css"));

        // Заголовок "Личные"
        HBox hPrivate = addBeginSection(vBox, "Личные:");
        HBox buttonsBoxPrivate = Util.getHBox(7);
        HBox.setMargin(buttonsBoxPrivate, new Insets(0, 10, 0, 10));
        hPrivate.getChildren().add(buttonsBoxPrivate);
        setHideHBox(hPrivate, buttonsBoxPrivate);

        Button buttonAddPlaylist = new Button("");
        buttonAddPlaylist.getStyleClass().add("button-icon");
        buttonAddPlaylist.setGraphic(new ImageView(Util.getPath("assets/images/icons/icon_add_1.png")));
        buttonsBoxPrivate.getChildren().add(buttonAddPlaylist);

        buttonAddPlaylist.setOnMouseClicked((event)->
        {
            ModuleAddPlaylist.create();
        });

        ArrayList<Playlist> playlists = app.audioArchive.getUserPlaylists();

        for (int i = 0; i < playlists.size(); i++)
        {
            Playlist playlist = playlists.get(i);
            if(playlist.getIdUser() == app.getUserId())
            {
                HBox hBox = getPrivatePlaylistBand(playlist);
                vBox.getChildren().add(hBox);
            }
        }

        // ===============================================================================================

        // Заголовок "Внешние"
        HBox hPublic = addBeginSection(vBox, "Внешние:");
        HBox buttonsBoxPublic = Util.getHBox(7);
        HBox.setMargin(buttonsBoxPublic, new Insets(0, 10, 0, 10));
        hPublic.getChildren().add(buttonsBoxPublic);
        setHideHBox(hPublic, buttonsBoxPublic);

        Button buttonSearchPlaylist = new Button("");
        buttonSearchPlaylist.getStyleClass().add("button-icon");
        buttonSearchPlaylist.setGraphic(new ImageView(Util.getPath("assets/images/icons/icon_add_1.png")));
        buttonsBoxPublic.getChildren().add(buttonSearchPlaylist);

        buttonSearchPlaylist.setOnMouseClicked((event)->
        {
            ModuleSearchPlaylists mSearch = new ModuleSearchPlaylists();
            mSearch.initAsFree();
        });

        for (int i = 0; i < playlists.size(); i++)
        {
            Playlist playlist = playlists.get(i);
            if(playlist.getIdUser() != app.getUserId())
            {
                HBox hBox = getPublicPlaylistBand(playlist);
                vBox.getChildren().add(hBox);
            }
        }

        // ===============================================================================================

        for (int i = 0; i < 3; i++)
        {
            Separator separator = new Separator();
            separator.setOrientation(Orientation.HORIZONTAL);
            vBox.getChildren().add(separator);
        }
    }

    private HBox addBeginSection(VBox vBox, String name)
    {
        for (int i = 0; i < 3; i++)
        {
            Separator separator = new Separator();
            separator.setOrientation(Orientation.HORIZONTAL);
            vBox.getChildren().add(separator);
        }

        HBox hBox = new HBox();
        hBox.setPrefWidth(Region.USE_COMPUTED_SIZE);
        hBox.setPrefHeight(Region.USE_COMPUTED_SIZE);
        hBox.setAlignment(Pos.CENTER);
        hBox.setPadding(new Insets(5, 5, 5, 5));

        Label labelName = new Label(name);
        labelName.setFont(Font.font("Calibri", FontWeight.BOLD, 14));
        hBox.getChildren().add(labelName);

        AnchorPane aP = new AnchorPane();
        aP.setPrefWidth(Region.USE_COMPUTED_SIZE);
        aP.setPrefHeight(Region.USE_COMPUTED_SIZE);
        aP.setMaxWidth(Double.MAX_VALUE);
        HBox.setHgrow(aP, Priority.ALWAYS);
        hBox.getChildren().add(aP);

        vBox.getChildren().add(hBox);

        Separator separator = new Separator();
        separator.setOrientation(Orientation.HORIZONTAL);
        vBox.getChildren().add(separator);

        return hBox;
    }

    private HBox getPrivatePlaylistBand(Playlist playlsit)
    {
        HBox hBox = new HBox();
        hBox.setAlignment(Pos.CENTER);
        hBox.setUserData(playlsit);
        hBox.setPadding(new Insets(5, 0, 5, 0));
        hBox.getStyleClass().add("list-item");
        hBox.setPrefWidth(Region.USE_COMPUTED_SIZE);
        hBox.setPrefHeight(Region.USE_COMPUTED_SIZE);
        hBox.setCursor(Cursor.HAND);

        String image = "assets/images/icons/icon_user_1.png";
        if(playlsit.getAccess() == 50) image = "assets/images/icons/icon_protected_1.png";
        if(playlsit.getAccess() == 90) image = "assets/images/icons/icon_public_1.png";

        ImageView imageView = new ImageView(Util.getPath(image));
        HBox.setMargin(imageView, new Insets(0, 0, 0, 10));
        hBox.getChildren().add(imageView);

        Label labelName = new Label(playlsit.getName());
        labelName.setMaxWidth(Double.MAX_VALUE);
        HBox.setHgrow(labelName, Priority.ALWAYS);
        HBox.setMargin(labelName, new Insets(0, 0, 0, 10));
        hBox.getChildren().add(labelName);

        HBox buttonsBox = new HBox();
        buttonsBox.setSpacing(7);
        buttonsBox.setPrefWidth(Region.USE_COMPUTED_SIZE);
        buttonsBox.setPrefHeight(Region.USE_COMPUTED_SIZE);
        buttonsBox.setVisible(false);
        HBox.setMargin(buttonsBox, new Insets(0, 10, 0, 10));
        hBox.getChildren().add(buttonsBox);

        hBox.setOnMouseEntered((event)->
        {
            buttonsBox.setVisible(true);
        });

        hBox.setOnMouseExited((event)->
        {
            buttonsBox.setVisible(false);
        });

        Button buttonAdd = new Button("");
        buttonAdd.getStyleClass().add("button-icon");
        buttonAdd.setGraphic(new ImageView(Util.getPath("assets/images/icons/icon_add_1.png")));
        buttonsBox.getChildren().add(buttonAdd);

        buttonAdd.setOnMouseClicked((event)->
        {
            ModuleAddAudiotrack.create(playlsit);
        });

        if(!playlsit.getIsRoot())
        {
            Button buttonEdit = new Button("");
            buttonEdit.getStyleClass().add("button-icon");
            buttonEdit.setGraphic(new ImageView(Util.getPath("assets/images/icons/icon_edit_1.png")));
            buttonsBox.getChildren().add(buttonEdit);

            buttonEdit.setOnMouseClicked((event)->
            {
                ModuleEditPlaylist.create(playlsit);
            });
        }

        hBox.setOnMouseClicked((event)->
        {
            app.moduleMain.mAudiotracksList.show(playlsit.getAudiotracks(), ModuleAudiotracksList.getControlBar(3, 2),playlsit);
        });

        return hBox;
    }

    private HBox getPublicPlaylistBand(Playlist playlsit)
    {
        HBox hBox = new HBox();
        hBox.setAlignment(Pos.CENTER);
        hBox.setUserData(playlsit);
        hBox.setPadding(new Insets(5, 0, 5, 0));
        hBox.getStyleClass().add("list-item");
        hBox.setPrefWidth(Region.USE_COMPUTED_SIZE);
        hBox.setPrefHeight(Region.USE_COMPUTED_SIZE);
        hBox.setCursor(Cursor.HAND);

        String image = "assets/images/icons/icon_playlist.png";

        ImageView imageView = new ImageView(Util.getPath(image));
        HBox.setMargin(imageView, new Insets(0, 0, 0, 10));
        hBox.getChildren().add(imageView);

        Label labelName = new Label(playlsit.getName());
        labelName.setMaxWidth(Double.MAX_VALUE);
        HBox.setHgrow(labelName, Priority.ALWAYS);
        HBox.setMargin(labelName, new Insets(0, 0, 0, 10));
        hBox.getChildren().add(labelName);

        HBox buttonsBox = new HBox();
        buttonsBox.setSpacing(7);
        buttonsBox.setPrefWidth(Region.USE_COMPUTED_SIZE);
        buttonsBox.setPrefHeight(Region.USE_COMPUTED_SIZE);
        buttonsBox.setVisible(false);
        HBox.setMargin(buttonsBox, new Insets(0, 10, 0, 10));
        hBox.getChildren().add(buttonsBox);

        Button buttonRemove = new Button("");
        buttonRemove.getStyleClass().add("button-icon");
        buttonRemove.setGraphic(new ImageView(Util.getPath("assets/images/icons/icon_remove_4.png")));
        buttonsBox.getChildren().add(buttonRemove);

        buttonRemove.setOnMouseClicked((event)->
        {
            app.reqRemoveUsingPlaylist(playlsit.getId(), null);
            app.audioArchive.removePlaylist(playlsit);
            update();
        });


        hBox.setOnMouseEntered((event)->
        {
            buttonsBox.setVisible(true);
        });

        hBox.setOnMouseExited((event)->
        {
            buttonsBox.setVisible(false);
        });

        hBox.setOnMouseClicked((event)->
        {
            app.audioArchive.fillPlaylist(playlsit, "", () ->
            {
                Platform.runLater(() ->
                {
                    app.moduleMain.mAudiotracksList.show(playlsit.getAudiotracks(), ModuleAudiotracksList.getControlBar(1, 3), playlsit);
                });
            }, ()->
            {
                Platform.runLater(()->
                {
                    ModuleInputPassword mPassword = ModuleInputPassword.create();
                    mPassword.setOnClose(() ->
                    {
                        String password = mPassword.getPassword();
                        app.reqAddUsingPlaylist(playlsit.getId(), password, null);
                    });
                });
            });
        });

        return hBox;
    }


    private void setHideHBox(HBox parent, HBox container)
    {
        container.setVisible(false);

        parent.setOnMouseEntered((event)->
        {
            container.setVisible(true);
        });

        parent.setOnMouseExited((event)->
        {
            container.setVisible(false);
        });
    }
}