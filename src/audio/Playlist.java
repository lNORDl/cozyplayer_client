package audio;

import org.json.simple.JSONObject;

import java.util.ArrayList;

/**
 * Created by NDS on 17.03.2015.
 */
public class Playlist
{
    private int id;
    private int idUser;
    private String name;
    private boolean isRoot;
    private int access;
    private boolean isPassword;
    private String passwordHash;
    private String date;
    private String tags;

    private String usingPassword;

    private ArrayList<Audiotrack> audiotracks;

    public Playlist(JSONObject info)
    {
        init(info);

        audiotracks = new ArrayList<Audiotrack>();

        //printInfo();
    }

    public void init(JSONObject info)
    {
        id = Integer.parseInt(info.get("id").toString());
        idUser = Integer.parseInt(info.get("id_user").toString());
        name = info.get("name").toString();
        isRoot = Boolean.parseBoolean(info.get("is_root").toString());
        access = Integer.parseInt(info.get("access").toString());
        isPassword = Boolean.parseBoolean(info.get("is_password").toString());
        passwordHash = info.get("password_hash").toString();
        date = info.get("date").toString();
        tags = info.get("tags").toString();
    }

    public Integer getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    public boolean getIsRoot()
    {
        return isRoot;
    }

    public int getAccess() { return access; }

    public boolean getIsPassword() { return isPassword; }

    public String getPasswordHash() { return passwordHash; }

    public String getTags()
    {
        return tags;
    }

    public int getIdUser()
    {
        return idUser;
    }

    public void printInfo()
    {
        System.out.println("============================================*****=============================================");
        System.out.println("Playlist: " + name + "; Size: " + audiotracks.size() + "; Audiotracks: ");
        for(int i = 0; i < audiotracks.size(); i++)
        {
            System.out.print("["+(i+1)+"] ");
            audiotracks.get(i).printInfo();
        }
        System.out.println("============================================*****=============================================");
    }

    public void clearAudiotracks()
    {
        audiotracks.clear();
    }

    public void addAudiotrack(Audiotrack audiotrack)
    {
        audiotracks.add(audiotrack);
    }

    public void addAudiotracks(ArrayList<Audiotrack> tracks)
    {
        for(int i = 0; i < tracks.size(); i++) addAudiotrack(tracks.get(i));
    }

    public void removeAudiotrack(Audiotrack track)
    {
        audiotracks.remove(track);
    }

    public ArrayList<Audiotrack> getAudiotracks()
    {
        return new ArrayList<Audiotrack>(audiotracks);
    }

    @Override
    public String toString()
    {
        return name;
    }
}
