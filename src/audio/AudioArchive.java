package audio;

import core.Application;
import javafx.application.Platform;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import util.*;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by NDS on 06.03.2015.
 */
public class AudioArchive
{
    private HashMap<Integer, Audiotrack> audiotracks;
    private HashMap<Integer, Playlist> playlists;

    private ArrayList<Playlist> playlistsList;

    private Playlist rootPlaylist;

    private Application app = Application.getInstance();

    private int count;

    public AudioArchive()
    {
        audiotracks = new HashMap<Integer, Audiotrack>();
        playlists = new HashMap<Integer, Playlist>();

        playlistsList = new ArrayList<Playlist>();
    }

    public void init(IDo task)
    {
        initUserPlaylists(()->
        {
            System.out.println("AudioArchive inited!");
            Platform.runLater(() ->
            {
                task.doTask();
            });
        });
    }

    //==========================AUDIOTRACKS==========================================================================

    public ArrayList<Audiotrack> getUserAudiotracks()
    {
        return new ArrayList<Audiotrack>(audiotracks.values());
    }

    public void uploadNewAudiotrack(File file, String name, String artist, float duration, String tags, boolean isPrivate, int idGenre, Playlist playlist)
    {
        NetworkWorker worker = app.reqAddAudiotrack(file, name, artist, duration, tags, isPrivate, idGenre, (String res) ->
        {
            System.out.println("Upload audiotrack");
            JSONObject json = Util.parseStringToJObject(res);
            if (json != null) {
                JSONObject audioInfo = (JSONObject) json.get("data");
                Audiotrack audio = new Audiotrack(audioInfo);
                audiotracks.put(audio.getId(), audio);

                rootPlaylist.addAudiotrack(audio);
                app.reqAddUsingAudiotrack(rootPlaylist.getId(), audio.getId(), null);

                if (playlist != null && playlist != rootPlaylist) {
                    playlist.addAudiotrack(audio);
                    app.reqAddUsingAudiotrack(playlist.getId(), audio.getId(), null);
                }
                Platform.runLater(()->
                {
                    app.moduleMain.mAudiotracksList.updatePlaylist();
                });
            }
        });
    }

    public void addAudiotrackInPlaylist(Audiotrack audio, Playlist playlist)
    {
        audiotracks.put(audio.getId(), audio);

        rootPlaylist.addAudiotrack(audio);
        app.reqAddUsingAudiotrack(rootPlaylist.getId(), audio.getId(), null);

        if (playlist != null && playlist != rootPlaylist) {
            playlist.addAudiotrack(audio);
            app.reqAddUsingAudiotrack(playlist.getId(), audio.getId(), null);
        }
    }

    public void removeAudiotrackInPlaylist(Audiotrack audio, Playlist playlist)
    {
        if (playlist != null) {
            playlist.removeAudiotrack(audio);
            app.reqRemoveUsingsAudiotrack(playlist.getId(), audio.getId(), null);
        }
        else
        {
            for(int i = 0; i < playlistsList.size(); i++)
            {
                if(playlistsList.get(i).getAudiotracks().indexOf(audio) != -1)
                {
                    playlistsList.get(i).removeAudiotrack(audio);
                }
            }
        }

        audiotracks = new HashMap<Integer, Audiotrack>();
        for(int i = 0; i < playlistsList.size(); i++)
        {
            ArrayList<Audiotrack> tracks = playlistsList.get(i).getAudiotracks();
            for(int j = 0; j < tracks.size(); j++)
            {
                Audiotrack track = audiotracks.get(tracks.get(j).getId());
                if(track == null) audiotracks.put(tracks.get(j).getId(), tracks.get(j));
            }
        }
    }

    //==========================PLAYLISTS============================================================================
    public void initUserPlaylists(IDo task)
    {
        new NetworkWorker("api/get_user_playlists", null, (String res)->
        {
            JSONArray json = Util.parseStringToJArray(res);
            for (int i = 0; i < json.size(); i++)
            {
                JSONObject info = (JSONObject) json.get(i);
                addPlaylist(new Playlist(info));
            }

            count = playlists.size();

            ArrayList<Playlist> playlists = getUserPlaylists();

            for(int i = 0; i < playlists.size(); i++)
            {
                Playlist playlist = playlists.get(i);
                app.reqGetPlaylistAudiotracks(playlist.getId(), "", (String res1)->
                {
                    ArrayList<Audiotrack> tracks = Util.completeAudiotracksFromInfo(audiotracks, (JSONArray) Util.parseStringToJObject(res1).get("data"));
                    playlist.addAudiotracks(tracks);

                    count --;
                    if(count == 0) task.doTask();
                });
            }

            new NetworkWorker("/api/get_usings_playlists", null, (String res1)->
            {
                JSONArray json1 = Util.parseStringToJArray(res1);
                for (int i = 0; i < json1.size(); i++)
                {
                    JSONObject info = (JSONObject) json1.get(i);
                    addPlaylist(new Playlist(info));
                }
            });
        });
    }

    public void addPlaylist(Playlist playlist)
    {
        playlists.put(playlist.getId(), playlist);
        playlistsList.add(playlist);
        if(playlist.getIsRoot()) rootPlaylist = playlist;
    }

    public void removePlaylist(Playlist playlist)
    {
        playlistsList.remove(playlist);
        playlists.remove(playlist.getId());
    }

    public ArrayList<Playlist> getUserPlaylists()
    {
        return new ArrayList<Playlist>(playlistsList);
    }

    public Playlist getRootPlaylist()
    {
        return rootPlaylist;
    }

    public void fillPlaylist(Playlist playlist, String password, IDo onComplete, IDo onEror)
    {
        app.reqGetPlaylistAudiotracks(playlist.getId(), password, (String res)->
        {
            if(!Util.isResponseError(res))
            {
                playlist.clearAudiotracks();
                JSONArray json = (JSONArray) Util.parseStringToJObject(res).get("data");
                for (int i = 0; i < json.size(); i++)
                {
                    Audiotrack audio = new Audiotrack((JSONObject) json.get(i));
                    if (audiotracks.get(audio.getId()) != null) audio = audiotracks.get(audio.getId());
                    playlist.addAudiotrack(audio);
                }

                if(onComplete != null) onComplete.doTask();
            }
            else if(onEror != null) onEror.doTask();
        });
    }

    public Audiotrack getUserAudiotrackById(int id)
    {
        return audiotracks.get(id);
    }

    public Playlist getUserPlaylistById(int id)
    {
        return playlists.get(id);
    }
}
