package audio;

import core.Application;
import org.json.simple.JSONObject;

/**
 * Created by NDS on 06.03.2015.
 */
public class Audiotrack
{
    private int id;
    private String name;
    private String artist;
    private float duration;
    private String path;
    private String tags;
    private boolean isPrivate;
    private int idUser;

    private boolean isPlay;

    private Application app = Application.getInstance();

    public Audiotrack(JSONObject info)
    {
        id = Integer.parseInt(info.get("id").toString());
        name = info.get("name").toString();
        artist = info.get("artist").toString();
        duration = Float.parseFloat(info.get("duration").toString());
        path = info.get("path").toString();
        tags = info.get("tags").toString();
        isPrivate = Boolean.parseBoolean(info.get("is_private").toString());
        idUser = Integer.parseInt(info.get("id_user").toString());
    }

    public void printInfo()
    {
        System.out.println("Audiotrack id="+id+": "+name+", "+artist);
    }

    public int getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    public String getArtist()
    {
        return artist;
    }

    public float getDuration()
    {
        return duration;
    }

    public String getPath()
    {
        return path;
    }

    public String getTags(){ return tags; }

    public boolean getIsPrivate()
    {
        return isPrivate;
    }

    public int getIdUser()
    {
        return idUser;
    }


    public void setPlay(boolean v)
    {
        isPlay = v;
    }

    public boolean getPlay()
    {
        return isPlay;
    }
}
