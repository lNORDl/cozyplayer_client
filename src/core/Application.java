package core;

import audio.AudioArchive;
import fxModules.*;
import javafx.application.Platform;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import javafx.stage.WindowEvent;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.simple.JSONArray;
import util.*;

/**
 * Created by NDS on 18.01.2015.
 */
public class Application
{
    private static Application instance;

    public ModuleMain moduleMain;
    public AudioArchive audioArchive;
    public ModulePlayer player;

    private Stage stage;
    private ArrayList<BasicModule> modules;

    private int userId;
    private int userAccess;

    private JSONArray genres;

    public Application()
    {
        instance = this;
    }

    public static Application getInstance()
    {
        if(instance != null) return instance;
        else return  new Application();
    }

    public void init(Stage s) throws SQLException
    {
        stage = s;

        modules = new ArrayList<BasicModule>();

        ModuleLogin mLogin = new ModuleLogin();
        mLogin.initAsModal(stage);
    }

    public void startApplication(int id, int access)
    {
        System.out.println("Applications started! User Id: " + id + "; User Access: " + access + ";");
        userId = id;
        userAccess = access;

        new NetworkWorker("/api/get_genres", null, (String res)->
        {
            genres = Util.parseStringToJArray(res);
        });

        moduleMain = new ModuleMain();
        moduleMain.initAsModal(stage);

        audioArchive = new AudioArchive();
        audioArchive.init(()->
        {
            moduleMain.mPlaylistsMenu.update();
            moduleMain.mAudiotracksList.show(audioArchive.getRootPlaylist().getAudiotracks(), ModuleAudiotracksList.getControlBar(3, 2), audioArchive.getRootPlaylist());
        });
    }

    public void addModule(BasicModule module)
    {
        int i = modules.indexOf(module);
        if(i == -1) modules.add(module);
    }

    public void removeModule(BasicModule module)
    {
        int i = modules.indexOf(module);
        if(i != -1) modules.remove(i);
    }

    public BasicModule getModuleByID(Util.ModuleID id)
    {
        for(int i = 0; i < modules.size(); i++)
        {
            //System.out.println(modules.get(i).getId() + ":" + id);
            if(modules.get(i).getId() == id) return modules.get(i);
        }

        return null;
    }

    public void downloadAudiotrack(String name, String path)
    {
        reqDownloadAudiotrack(path, (byte[] res)->
        {
            if(res != null)
            {
                File file = new File(name);
                try
                {
                    FileOutputStream os = new FileOutputStream(file);
                    os.write(res);
                    os.close();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        });
    }

    public NetworkWorker reqDoAdminQuery(String query, IServerResponseString res)
    {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("query", query));

        NetworkWorker worker = new NetworkWorker("/api/do_admin_query", params, res);

        return worker;
    }

    public NetworkWorker reqGetAudiotracksStatistics(String dateStart, String dateEnd, IServerResponseString res)
    {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("date_start", dateStart));
        params.add(new BasicNameValuePair("date_end", dateEnd));

        NetworkWorker worker = new NetworkWorker("/api/get_audiotracks_statistics", params, res);

        return worker;
    }

    public NetworkWorker reqGetUsersStatistics(String dateStart, String dateEnd, IServerResponseString res)
    {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("date_start", dateStart));
        params.add(new BasicNameValuePair("date_end", dateEnd));

        NetworkWorker worker = new NetworkWorker("/api/get_users_statistics", params, res);

        return worker;
    }

    public NetworkWorker reqGetTopGenres(int count, String dateStart, String dateEnd, IServerResponseString res)
    {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("count", Integer.toString(count)));
        params.add(new BasicNameValuePair("date_start", dateStart));
        params.add(new BasicNameValuePair("date_end", dateEnd));

        NetworkWorker worker = new NetworkWorker("/api/get_top_genres", params, res);

        return worker;
    }

    public NetworkWorker reqGetTopAudiotracks(int count, String dateStart, String dateEnd, IServerResponseString res)
    {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("count", Integer.toString(count)));
        params.add(new BasicNameValuePair("date_start", dateStart));
        params.add(new BasicNameValuePair("date_end", dateEnd));

        NetworkWorker worker = new NetworkWorker("/api/get_top_audiotracks", params, res);

        return worker;
    }

    public NetworkWorker reqRemoveUsingPlaylist(int idPlaylist, IServerResponseString res)
    {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("id_playlist", Integer.toString(idPlaylist)));

        NetworkWorker worker = new NetworkWorker("/api/remove_using_playlist", params, res);

        return worker;
    }

    public NetworkWorker reqAddUsingPlaylist(int idPlaylist, String password, IServerResponseString res)
    {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("id_playlist", Integer.toString(idPlaylist)));
        params.add(new BasicNameValuePair("password", password));

        NetworkWorker worker = new NetworkWorker("/api/add_using_playlist", params, res);

        return worker;
    }

    public NetworkWorker reqAddPopularity(int idAudiotrack, String date, IServerResponseString res)
    {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("id_audiotrack", Integer.toString(idAudiotrack)));
        params.add(new BasicNameValuePair("date", date));

        NetworkWorker worker = new NetworkWorker("/api/add_popularity", params, res);

        return worker;
    }

    public NetworkWorker reqDownloadAudiotrack(String path, IServerResponseBytes res)
    {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("path", path));

        NetworkWorker worker = new NetworkWorker("/api/audiotrack_download", params, res);

        return worker;
    }

    public NetworkWorker reqSearchPlaylists(String tags, Boolean isFree, IServerResponseString res)
    {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("tags", Util.getTags(tags)));
        params.add(new BasicNameValuePair("is_free", Boolean.toString(isFree)));

        NetworkWorker worker = new NetworkWorker("/api/search_playlists", params, res);

        return worker;
    }

    public NetworkWorker reqSearchAudiotracks(String tags, int idGenre, String date, IServerResponseString res)
    {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("tags", Util.getTags(tags)));
        params.add(new BasicNameValuePair("id_genre", Integer.toString(idGenre)));
        params.add(new BasicNameValuePair("date", date));

        NetworkWorker worker = new NetworkWorker("/api/search_audiotracks", params, res);

        return worker;
    }

    public NetworkWorker reqRemoveUsingsAudiotrack(int idPlaylist, int idAudiotrack, IServerResponseString res)
    {
        JSONArray audiotracks = new JSONArray();
        audiotracks.add(idAudiotrack);
        return reqRemoveUsingsAudiotracks(idPlaylist, audiotracks, res);
    }

    public NetworkWorker reqRemoveUsingsAudiotracks(int idPlaylist, JSONArray audiotracks, IServerResponseString res)
    {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("audiotracks", audiotracks.toString()));
        params.add(new BasicNameValuePair("id_playlist", Integer.toString(idPlaylist)));

        NetworkWorker worker = new NetworkWorker("/api/remove_usings_audiotracks", params, res);

        return worker;
    }

    public NetworkWorker reqAddUsingAudiotrack(int idPlaylist, int idAudiotrack, IServerResponseString res)
    {
        JSONArray audiotracks = new JSONArray();
        audiotracks.add(idAudiotrack);
        return reqAddUsingsAudiotracks(idPlaylist, audiotracks, res);
    }

    public NetworkWorker reqAddUsingsAudiotracks(int idPlaylist, JSONArray audiotracks, IServerResponseString res)
    {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("audiotracks", audiotracks.toString()));
        params.add(new BasicNameValuePair("id_playlist", Integer.toString(idPlaylist)));

        NetworkWorker worker = new NetworkWorker("/api/add_usings_audiotracks", params, res);

        return worker;
    }
    public NetworkWorker reqAddPlaylist(String name, int access, boolean isPassword, String password, String tags, IServerResponseString res)
    {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("name", name));
        params.add(new BasicNameValuePair("access", Integer.toString(access)));
        params.add(new BasicNameValuePair("is_password", Boolean.toString(isPassword)));
        params.add(new BasicNameValuePair("password", password));
        params.add(new BasicNameValuePair("tags", Util.getTags(tags)));

        NetworkWorker worker = new NetworkWorker("/api/add_playlist", params, res);

        return worker;
    }

    public NetworkWorker reqRemovePlaylist(int idPlaylist, IServerResponseString res)
    {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("playlist_id", Integer.toString(idPlaylist)));

        NetworkWorker worker = new NetworkWorker("/api/remove_playlist", params, res);

        return worker;
    }

    public NetworkWorker reqEditPlaylist(int idPlaylist, String name, int access, boolean isPassword, String password, boolean isChangePassword, String tags, IServerResponseString res)
    {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("id_playlist", Integer.toString(idPlaylist)));
        params.add(new BasicNameValuePair("name", name));
        params.add(new BasicNameValuePair("access", Integer.toString(access)));
        params.add(new BasicNameValuePair("is_password", Boolean.toString(isPassword)));
        params.add(new BasicNameValuePair("password", password));
        params.add(new BasicNameValuePair("is_change_password", Boolean.toString(isChangePassword)));
        params.add(new BasicNameValuePair("tags", Util.getTags(tags)));

        NetworkWorker worker = new NetworkWorker("/api/edit_playlist", params, res);

        return worker;
    }

    public NetworkWorker reqGetPlaylistAudiotracks(int idPlaylist, String password, IServerResponseString res)
    {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("id_playlist", Integer.toString(idPlaylist)));
        params.add(new BasicNameValuePair("password", password));

        NetworkWorker worker = new NetworkWorker("/api/get_playlist_audiotracks", params, res);

        return worker;
    }

    public NetworkWorker reqRemoveAudiotrack(int idAudiotrack, IServerResponseString res)
    {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("id_audiotrack", Integer.toString(idAudiotrack)));

        NetworkWorker worker = new NetworkWorker("api/remove_audiotrack", params, res);

        return worker;
    }

    public NetworkWorker reqAddAudiotrack(File file, String name, String artist, float duration, String tags, boolean isPrivate, int idGenre, IServerResponseString res)
    {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("name", name));
        params.add(new BasicNameValuePair("artist", artist));
        params.add(new BasicNameValuePair("tags", Util.getTags(tags)));
        params.add(new BasicNameValuePair("duration", Float.toString(duration)));
        params.add(new BasicNameValuePair("isPrivate", Boolean.toString(isPrivate)));
        params.add(new BasicNameValuePair("id_genre", Integer.toString(idGenre)));

        NetworkWorker worker = new NetworkWorker("api/add_audiotrack", params, file, res);

        return worker;
    }

    public NetworkWorker reqActivate(String login, int code, IServerResponseString res)
    {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("login", login));
        params.add(new BasicNameValuePair("code", Integer.toString(code)));

        NetworkWorker worker = new NetworkWorker("api/activate", params, res);

        return worker;
    }

    public NetworkWorker reqRegistration(String login, String password, String email, IServerResponseString res)
    {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("login", login));
        params.add(new BasicNameValuePair("password", password));
        params.add(new BasicNameValuePair("email", email));

        NetworkWorker worker = new NetworkWorker("api/registration", params, res);

        return worker;
    }

    public NetworkWorker reqLogin(String login, String password, IServerResponseString res)
    {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("login", login));
        params.add(new BasicNameValuePair("password", password));
        NetworkWorker worker = new NetworkWorker("api/login", params, res);

        return worker;
    }

    public JSONArray getGenres()
    {
        return genres;
    }

    public int getUserId()
    {
        return userId;
    }

    public int getUserAccess()
    {
        return userAccess;
    }

    public Stage getStage()
    {
        return stage;
    }

    public void close()
    {
        for(int i = 0; i < modules.size(); i++)
        {
            modules.get(i).destroy();
        }
        stage.close();
        Platform.exit();
        System.exit(0);
    }
}
