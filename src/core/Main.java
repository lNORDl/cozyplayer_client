package core;

import fxModules.ModuleMain;
import javafx.application.Application;
import javafx.stage.Stage;
import util.NetworkWorker;
import util.Util;

public class Main extends Application {

    private ModuleMain moduleMain;

    @Override
    public void start(Stage stage) throws Exception
    {
        Util.init();
        NetworkWorker.init();

        core.Application app = new core.Application();
        app.init(stage);
    }


    public static void main(String[] args)
    {
        launch(args);
    }
}
