package util;

import audio.Audiotrack;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by NDS on 17.01.2015.
 */
public class Util
{
    public static enum ModuleID {M_NONE, M_MAIN, M_PLAYER, M_LOGIN, M_LOADER, M_ADD_AUDIOTRACK, M_AUDIO_ARCHIVE};

    public static HashMap<String, Object> config = new HashMap<String, Object>();

    public static void init()
    {
        config.put("host", "localhost:3000");
        config.put("download", "download");
        config.put("ACCESS_ADMIN", 3);
        config.put("ACCESS_USER", 1);
    }

    public static String getServerUrl()
    {
        return "http://"+config.get("host")+"/";
    }
    public static String getDownloadUrl()
    {
        return config.get("download") + "/";
    }
    public static int getAccessUser()
    {
        return Integer.parseInt(config.get("ACCESS_USER").toString());
    }
    public static int getAccessAdmin()
    {
        return Integer.parseInt(config.get("ACCESS_ADMIN").toString());
    }

    public static boolean isResponseError(String res)
    {
        JSONObject json = parseStringToJObject(res);
        if(json != null)
        {
            Object o = json.get("status");
            String s = null;
            if(o != null) s = o.toString();
            if(s == null) return false;
            return s.compareTo("ERROR") == 0;
        }

        return false;
    }

    public static JSONObject parseStringToJObject(String s)
    {
        JSONObject json = null;
        try {
            json = (JSONObject) new JSONParser().parse(s);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }

        return json;
    }

    public static JSONArray parseStringToJArray(String s)
    {
        JSONArray json = null;
        try {
            json = (JSONArray) new JSONParser().parse(s);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }

        return json;
    }

    public static boolean checkString(String str, int minL, int maxL)
    {
        str = str.trim();
        if(str.length() > maxL || str.length() < minL) return false;
        return true;
    }

    public static String getPath(String p)
    {
        return new File(p).toURI().toString();
    }

    public static String utf8(String s) {
        String out = null;
        try {
            out = new String(s.getBytes("UTF-8"), "ISO-8859-1");
        } catch (java.io.UnsupportedEncodingException e)
        {
            e.printStackTrace();
            return null;
        }
        return out;
    }

    public static void addParamsAsPart(MultipartEntityBuilder builder, List<NameValuePair> params)
    {
        if(params != null)
        {
            for(int i = 0; i < params.size(); i++)
            {
                BasicNameValuePair param = (BasicNameValuePair) params.get(i);
                try {
                    builder.addPart(param.getName(), new StringBody(param.getValue(), Charset.forName("UTF-8")));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static String getAudiotrackPath(String p)
    {
        return getServerUrl() + "api/get_audiotrack?name=" + p;
    }

    public static class LoadingStructure
    {
        public NetworkWorker loading;
        public String label;

        public LoadingStructure(NetworkWorker ld, String lb)
        {
            loading = ld;
            label = lb;
        }
    }

    public static String getTags(String s)
    {
        String[] tags = s.split(",");
        JSONArray t = new JSONArray();
        for (int i = 0; i < tags.length; i++)
        {
            String tag = tags[i].trim().toLowerCase();
            if(tag.length() > 1) t.add(tag);
        }

        return t.toJSONString();
    }

    public static float soundTimeToMinutes(int seconds)
    {
        float minutes = (int) (seconds / 60);
        minutes += (seconds - (60 * minutes)) / 100;
        return minutes;
    }

    public static String getMD5(String md5) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(md5.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
        }
        return null;
    }

    public static ArrayList<Audiotrack> createAudiotracksFromInfo(JSONArray json)
    {
        ArrayList<Audiotrack> audiotracks = new ArrayList<Audiotrack>();
        for(int i = 0; i < json.size(); i++)
        {
            JSONObject info = (JSONObject) json.get(i);
            Audiotrack track = new Audiotrack(info);
            audiotracks.add(track);
        }

        return audiotracks;
    }

    public static ArrayList<Audiotrack> completeAudiotracksFromInfo(HashMap<Integer, Audiotrack> audiotracks, JSONArray json)
    {
        ArrayList<Audiotrack> tracks = createAudiotracksFromInfo(json);
        for (int i = 0; i < tracks.size(); i++)
        {
            Audiotrack newTrack = tracks.get(i);
            Audiotrack archiveTrack = audiotracks.get(newTrack.getId());
            if(archiveTrack != null) tracks.set(i, archiveTrack);
            else audiotracks.put(newTrack.getId(), newTrack);
        }
        return tracks;
    }

    public static HBox getHBox(int spacing)
    {
        HBox hBox = new HBox();
        hBox.setSpacing(spacing);
        hBox.setPrefWidth(Region.USE_COMPUTED_SIZE);
        hBox.setPrefHeight(Region.USE_COMPUTED_SIZE);
        hBox.setMaxWidth(Double.MAX_VALUE);
        hBox.setMaxHeight(Double.MAX_VALUE);

        return hBox;
    }

    public static Object lookup(JSONArray data, String keyField, String keyValue, String lField)
    {
        for(int i = 0; i < data.size(); i++)
        {
            JSONObject json = (JSONObject) data.get(i);
            if(json != null)
            {
                Object o = json.get(keyField);
                if(o != null && o.equals(keyValue)) return json.get(lField);
            }
        }

        return null;
    }

    public static int getRandomInt(int min, int max) {

        Random rand = new Random();
        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }
}
