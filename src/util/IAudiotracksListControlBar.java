package util;

import audio.Audiotrack;
import fxModules.ModuleAudiotracksList;
import javafx.scene.layout.HBox;

/**
 * Created by NDS on 02.06.2015.
 */
public interface IAudiotracksListControlBar
{
    HBox getControlBar(ModuleAudiotracksList module, Audiotrack track);
}
