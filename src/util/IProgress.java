package util;

/**
 * Created by NDS on 03.02.2015.
 */
public interface IProgress
{
    void onProgress(float progress);
}
