package util;

import audio.Audiotrack;
import audio.Playlist;
import fxModules.ModuleAudiotracksList;
import fxModules.ModulePlaylistsList;
import javafx.scene.layout.HBox;

/**
 * Created by NDS on 02.06.2015.
 */
public interface IPlaylistsListControlBar
{
    HBox getControlBar(ModulePlaylistsList module, Playlist playlist, HBox band);
}
