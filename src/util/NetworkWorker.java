package util;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by NDS on 28.02.2015.
 */
public class NetworkWorker
{
    private enum Types {POST_REQUES, FILE_UPLOAD};

    private Thread thread;
    private Runnable method;

    private IServerResponseString responseString;
    private IServerResponseBytes responseBytes;

    private Request req;
    private Response res;
    private HttpPost post;

    private IProgress progressHandler;
    private float loadProgress = 0;

    private Types type;

    public static void init()
    {

    }

    public NetworkWorker(String path, List<NameValuePair> params, IServerResponseBytes res)
    {
        responseBytes = res;

        method = ()->
        {
            Response r = postRequest(path, params);
            byte[] b = null;
            if(r != null) {
                try {
                    b = r.returnContent().asBytes();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(responseBytes != null) responseBytes.response(b);
        };

        init(Types.POST_REQUES);
    }

    public NetworkWorker(String path, List<NameValuePair> params, IServerResponseString res)
    {
        responseString = res;

        method = ()->
        {
            Response r = postRequest(path, params);
            String s = null;
            if(r != null) {
                try {
                    s = r.returnContent().asString();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if(responseString != null) responseString.response(s);
            destroy();
        };

        init(Types.POST_REQUES);
    }

    public NetworkWorker(String path, List<NameValuePair> params, File file, IServerResponseString res)
    {
        responseString = res;

        method = ()->
        {
            String r = postRequest(path, params, file);

            if(responseString != null) responseString.response(r);
            destroy();
        };

        init(Types.FILE_UPLOAD);
    }

    private void init(Types t)
    {
        type = t;

        thread = new Thread(method);
        thread.start();
    }

    private void destroy()
    {
        thread = null;
        method = null;
        responseString = null;
        responseBytes = null;
        progressHandler = null;

        if(req != null) req.abort();
        if(post != null) post.abort();
        if(res != null) res.discardContent();
        res = null;
        req = null;
        post = null;
    }

    private void onProgress(float progress)
    {
        loadProgress = progress;
        if(progressHandler != null) progressHandler.onProgress(loadProgress);
    }

    public void setProgressHandler(IProgress handler)
    {
        if(type == Types.FILE_UPLOAD)
        {
            progressHandler = handler;
            progressHandler.onProgress(loadProgress);
        }
    }

    // Посылает POST запрос серверу
    private Response postRequest(String path, List<NameValuePair> params)
    {
        post = new HttpPost(Util.getServerUrl() + path);
        System.out.println(Util.getServerUrl() + path);
        if(params != null)
        {
            try
            {
                post.setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
            }
            catch (UnsupportedEncodingException e)
            {
                e.printStackTrace();
            }
        }

        String response = null;

        req = Request.Post(Util.getServerUrl() + path).body(post.getEntity());
        res = null;
        try
        {
            res = req.execute();
        }
        catch (IOException e)
        {
            e.printStackTrace();
            return null;
        }

        return res;
    }

    // Посылает файл серверу
    private String postRequest(String path, List<NameValuePair> params, File file)
    {
        post = new HttpPost(Util.getServerUrl() + path);
        post.setHeader("Accept-Encoding", "UTF-8");
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        builder.addPart("file", new FileBody(file));
        Util.addParamsAsPart(builder, params);

        post.setEntity(builder.build());

        ProgressHttpEntityWrapper.ProgressCallback progressCallback = (progress) ->
        {
            onProgress(progress);
        };

        post.setEntity(new ProgressHttpEntityWrapper(builder.build(), progressCallback));

        String response = null;

        req = Request.Post(Util.getServerUrl() + path).body(post.getEntity());
        res = null;
        try {
            res = req.execute();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        if (res != null) {
            try {
                response = res.returnContent().asString();
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }

        return response;
    }
}
