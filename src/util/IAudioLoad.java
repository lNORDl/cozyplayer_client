package util;

import audio.Audiotrack;

import java.util.ArrayList;

/**
 * Created by NDS on 18.03.2015.
 */
public interface IAudioLoad
{
    void onLoad(ArrayList<Audiotrack> tracks);
}
